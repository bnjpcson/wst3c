<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CertificatesController extends Controller
{
    //
    public function displayCertificates(Request $request){

        // $articles =DB::table('articles')
        //         ->join('categories', 'articles.id', '=', 'categories.id')
        //         ->join('users', 'users.id', '=', 'articles.user_id')
        //         ->select('articles.id','articles.title','articles.body','users.username', 'category.name')
        //         ->get();

        $certs = DB::table('certificates')
                ->join('seminars', 'certificates.seminar_id', '=', 'seminars.id')
                ->select('certificates.id', 'seminars.title', 'certificates.id', 'certificates.awardee', 'certificates.img_path', 'certificates.token', 'certificates.creator', 'certificates.created_at')
                ->get();

        $emails = Email::all();


        if ($request->session()->has('username')) {
            return view('certificates', ['certificates' => $certs, 'emails' => $emails]);
        }else{
            return redirect('admin');
        }
    }
}
