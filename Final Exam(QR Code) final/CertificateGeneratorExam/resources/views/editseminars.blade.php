@extends('master')

@section('title')
<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<link href="/css/A_home.css" rel="stylesheet" type="text/css">

    
<title>Seminars</title>

<style type="text/css">
  .hello{
    font-size: 30px;
  }
  .active::before{
    background: white;
  }
  .active{
    color: #082b54;
  }
</style>
@endsection

@section('content')

<header class="header" style="height:130px;">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show my-auto">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="{{route('seminars')}}" class="btn topnav__link active">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="{{route('templates')}}" class="btn topnav__link">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="{{route('generator')}}" class="btn topnav__link">Generator</a>
    </li>
    <li class="topnav__item">
      <a href="{{route('certs')}}" class="btn topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="{{route('admins')}}" class="btn topnav__link">Admins</a>
    </li>
    <li class="topnav__item">
      <div class="dropdown">
        <button class="btn topnav__link dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
          My Profile
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
          <li><a class="dropdown-item" href="{{asset('manual/manual.pdf');}}">User's Manual</a></li>
          <li><a href="{{route('logout')}}" class="dropdown-item"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
        </ul>
      </div>
    </li>
  </ul>
  <div class="p-1 position-absolute bottom-0 end-0 text-end pe-3">
    <span id='ct6'></span>
  </div>
</header>
  <span id='ct6' class="p-1 float-end" style="background-color: #FFBF00;"></span>

  <div class="container">
    @if(Session::has('success-seminar'))
    <div class="alert alert-success">
        {{ Session::get('success-seminar') }}
        @php
            Session::forget('success-seminar');
        @endphp
    </div>
    @endif
  </div>
  
  
  
  <div class="card mb-3 col-sm-6 mx-auto mt-5">
    {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
  
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  
    <div class="card-body">
      <div class = "container col-sm-12">
      @foreach ($seminars as $seminar)
      <form action="/seminars/edit/{{$seminar->id}}" method = "post" enctype="multipart/form-data">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="form-row">
                <div class="input-group mb-3">
                    <span class="input-group-text text-dark fw-bold">Title</span>
                    <input type="text" name="title" class="form-control" value="{{$seminar->title}}">
                </div>
                <div class="input-group">
                  <span class="input-group-text text-dark fw-bold">Date</span>
                  <input type="date" class="form-control" name="sdate" value="{{$seminar->sdate}}"> 
                  <span class="p-2 fw-bold">TO</span>
                  <input type="date" class="form-control" name="edate" value="{{$seminar->edate}}">
                  <span aria-describedby="dateHelp"></span>
                  
                </div>
                <div id="dateHelp" class="form-text mb-3">The date must be the day after tomorrow</div>
                <div class="input-group mb-3">
                  <span  class="input-group-text text-dark fw-bold">Status</span>
                  <select class="form-select" aria-label="Default select example" name="status">
                    <option value="1"
                    @if ($seminar->status == "1")
                        selected
                    @endif
                    >Active</option>
                    <option value="0"
                    @if ($seminar->status == "0")
                        selected
                    @endif
                    >Inactive</option>
                  </select>
                </div> 
                <div class="input-group mb-3">
                    <span class="input-group-text text-dark fw-bold">Venue</span>
                    <input type="text" name="venue" class="form-control"  value="{{$seminar->venue}}">
                </div>
  
  
                <center>
                  <button type="submit" class="btn btn-block mx-auto text-white" style="background-color: #082b54">Update Seminar</button>
                </center>
            </div>
        </form>
      @endforeach
    </div>
    </div>
  </div>

@endsection
