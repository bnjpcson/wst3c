<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
        <form action="" style="width: 50%; margin:auto;">
            <fieldset>
                <legend>Personal Information</legend>
                <label for="">Firstname :</label><br>
                <input type="text" name="firstname" placeholder="Enter your firstname">
                <br><br>
                <label for="">Lastname :</label><br>
                <input type="text" name="lastname" placeholder="Enter your lastname">
                <br><br>
                <label for="">Username :</label><br>
                <input type="text" name="username" placeholder="Enter your username">
                <br><br>
                <label for="">Password :</label><br>
                <input type="password" style="border: 5px solid red;" name="password" placeholder="Enter your password">
                <br><br>
                <textarea name="textarea" id="" cols="50" rows="10"></textarea><br>
                <label for="">Birthdate :</label>
                <input type="date" name="birthdate"><br>
                <label for="" style="color:red;">Town </label>
                <select name="town">
                    <option value="">Nancayasan</option>
                </select><br>
                <label for="">Browser :</label>
                <input type="text" name="browser"><br>
                <button style="padding: 10px; color:white; background-color: green; border: 2px solid red">Click Me</button>
                <button>Reset</button>
                <button>pindot me</button>
            </fieldset>
        </form>
    </body>
</html>
