**ACTIVITY 1: CREATE FIRST LARAVEL BLADE CODE**

---

## Laravel Folder

This is my complete laravel folder for Activity 1.

---

## activity1.blade.php

This is the blade code containing the contents of the given output. Please add it in this path:

C:\xampp\htdocs\projects\wst3c\wst3c\activity1\laravel\resources\views\activity1.blade.php

---

## web.php

I added codes in this file to enable or to easily access my activity1 blade code. Put this in this path:

C:\xampp\htdocs\projects\wst3c\wst3c\activity1\laravel\routes\web.php



