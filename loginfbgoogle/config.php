<?php

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

define('APP_ID', '443505927517513');
define('APP_SECRET', 'b4ccd74b2a3feff5ff296a8a30cb09b5');
define('API_VERSION', 'v2.5');
define('FB_BASE_URL', 'http://localhost/projects/wst3c/wst3c/loginfbgoogle/login.php');

if(!session_id()){
    session_start();
}

require_once(__DIR__.'/Facebook/autoload.php');

$fb = new Facebook([
    'app_id' => APP_ID,
    'app_secret' => APP_SECRET,
    'default_graph_version' => API_VERSION
]);

$fb_helper = $fb->getRedirectLoginHelper();

try{
    if(isset($_SESSION['facebook_access_token'])){
        $accessToken = $_SESSION['facebook_access_token'];
    }else{
        $accessToken = $fb_helper->getAccessToken();
    }
}catch(FacebookResponseException $e){
    echo 'Facebook API Error: '.$e->getMessage();
    exit;
}catch(FacebookSDKException $e){
    echo 'Facebook SDK Error: '.$e->getMessage();
    exit;
}


?>