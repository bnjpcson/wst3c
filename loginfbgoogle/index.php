<?php
    session_start();

    if(!isset($_SESSION['user_type'])) header("Location: login.php");

    $name = "Benjie Pecson";
    $date = "March 06, 2022";
    $ys = "3C";
    $time = "07:50 PM";


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-signin-client_id" content="352979315290-4lgbmagc19cvmmmahu4pceatfonlkp8a.apps.googleusercontent.com">
    <title>Login FB / Google API Script</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>




   
</head>
<body>
    <div class="container my-5 w-50 p-3 text-primary">
        

        <div class="border border-dark my-3 p-4">
            <div class="row">
                <div class="col">
                    <h5> Name: <?php echo $_SESSION['name'];?></h5>
                </div>
                <div class="col text-end">
                    <h5><?php echo $date;?></h5>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <h5>Email: <?php echo $_SESSION['email'];?></h5>
                </div>
                <div class="col text-end">
                    <h5><?php echo $time;?></h5>
                </div>
            </div>
            <div class="row my-2">
                <h3 class="text-center text-danger">WELCOME <?php echo $_SESSION['user_type']; ?></h3>
            </div>
            <div class="row my-2">
                <div class="col-sm-6 mx-auto">
                    <?php
                        if($_SESSION['user_type'] == "GOOGLE"){
                    ?>
                        <a class="nav-link btn btn-success btn-sm text-light" href="#" onclick="signOut();">Logout</a>
                        
                        <script>
                            function signOut() {
                                var auth2 = gapi.auth2.getAuthInstance();
                                auth2.signOut().then(function () {
                                console.log('User signed out.');
                                });
                                <?php
                                    session_unset();
                                    session_destroy();
                                ?>
                                window.location.href = "index.php";
                            }
                            function onLoad() {
                                gapi.load('auth2', function() {
                                    gapi.auth2.init();
                                });
                            }
                        </script>
                    <?php        
                        }else{
                    ?>
                        <a class="nav-link btn btn-success btn-sm text-light" href="logout.php">Logout</a>
                    <?php
                        }
                    ?>
                    
                </div>
                
            </div>
        </div>
    <!-- -------------------Google Platform Library------------------- -->
    <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
    <!-- -------------------Google Platform Library------------------- -->

    </div>
</body>
</html>