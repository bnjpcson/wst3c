<?php


    require_once 'config.php';

    $permissions = ['email']; //optional

    if(isset($accessToken)){

        if(!isset($_SESSION['facebook_access_token'])){
            $_SESSION['facebook_access_token'] = (string) $accessToken;
            $oAuth2Client = $fb->getOAuth2Client();
            $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
            $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken; 
            $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
        }else{
            $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
        }

            
        //redirect the user to the index page if it has $_GET['code']
        if (isset($_GET['code'])) 
        {
            header('Location: ./');
        }
        
        
        try {
            $fb_response = $fb->get('/me?fields=name,first_name,last_name,email');
            $fb_response_picture = $fb->get('/me/picture?redirect=false&height=200');
            
            $fb_user = $fb_response->getGraphUser();
            $picture = $fb_response_picture->getGraphUser();
            
            $_SESSION['fb_user_id'] = $fb_user->getProperty('id');
            $_SESSION['fb_user_name'] = $fb_user->getProperty('name');
            $_SESSION['fb_user_email'] = $fb_user->getProperty('email');
            $_SESSION['fb_user_pic'] = $picture['url'];

            $_SESSION['name'] = $fb_user->getProperty('name');
            $_SESSION['user_type'] = "FACEBOOK";
            $_SESSION['email'] = $fb_user->getProperty('email');
            
            header('Location: index.php');
            
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Facebook API Error: ' . $e->getMessage();
            session_destroy();
            // redirecting user back to app login page
            header("Location: ./");
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK Error: ' . $e->getMessage();
            exit;
        }

    }else{
        $fb_login_url = $fb_helper->getLoginUrl(FB_BASE_URL, $permissions);
    }



    if(isset($_SESSION['user_type'])) header("Location: index.php");

    $name = "Benjie Pecson";
    $date = "March 06, 2022";
    $ys = "3C";
    $time = "07:50 PM";

    if(isset($_POST['submit'])){
        $_SESSION['name'] = "example";
        $_SESSION['user_type'] = "EXAMPLE";
        $_SESSION['email'] = "example@example.com";
        header("Location: index.php");
    }

    if(isset($_GET['google'])){
        if(!empty($_GET['name'])){
            $_SESSION['name'] = $_GET['name'];
        }
        if(!empty($_GET['email'])){
            $_SESSION['email'] = $_GET['email'];
        }
        $_SESSION['user_type'] = "GOOGLE";
        header("Location: index.php");
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-signin-client_id" content="352979315290-4lgbmagc19cvmmmahu4pceatfonlkp8a.apps.googleusercontent.com">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Login FB / Google API Script</title>
 

    <script>
        var name = "";
        var email = "";

        function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
        console.log('Name: ' + profile.getName());
        console.log('Image URL: ' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

        name = profile.getName();
        email = profile.getEmail();

    

        if(name != ""){
            window.location.href = "/projects/wst3c/wst3c/loginfbgoogle/login.php?google&name="+name+"&email="+email;
        }
        
        }

    </script>
</head>



<body>
    
<div class="container my-5 w-50 p-3 text-primary">
    <div class="row">
        <div class="col">
            <h5> Name: <?php echo $name;?></h5>
        </div>
        <div class="col text-end">
            <h5><?php echo $date;?></h5>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <h5>Year and Section: <?php echo $ys;?></h5>
        </div>
        <div class="col text-end">
            <h5><?php echo $time;?></h5>
        </div>
    </div>

    <div class="border border-dark my-3 p-2">
        <div class="row my-2">
            <div class="col-sm-5 mx-auto">
                <a href="<?php echo $fb_login_url;?>" class="btn btn-primary w-100"><i class="fa fa-facebook me-3"></i> Sign in with Facebook</a>
            </div>
        </div>
        <div class="row my-2">
            <div class="col-sm-5 mx-auto">
                <div class="g-signin2" data-width="250" data-height="40" data-longtitle="true" data-onsuccess="onSignIn"></div>
            </div>
        </div>

        <div class="row my-2">
            <div class="col text-center">
                <form action="" method="POST">
                    <div class="row">
                        <div class="col-sm-5 mx-auto">
                            <input type="submit" name="submit" value="Login"  class="form-control btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
        </div>

     
<!-- -------------------Google Platform Library------------------- -->
<script src="https://apis.google.com/js/platform.js" async defer></script>
    <!-- -------------------Google Platform Library------------------- -->
   
    </div>
</div>


<br>

    


    
</body>
</html>