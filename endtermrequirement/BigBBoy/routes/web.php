<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MessageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/welcome', function () {
//     return view('welcome');
// });




// Route::get('/gallery', function () {
//     return view('user.gallery');
// });

// Route::get('/about', function () {
//     return view('user.about');
// });

// Route::get('/contact', function () {
//     return view('user.contact');
// });


Route::get('/php', function () {
    echo phpinfo();
});
Route::get('/welcome', function () {
    return view('welcome');
});


Route::get('/',[HomeController::class, 'home']);
Route::get('/gallery',[GalleryController::class, 'gallery']);
Route::get('/about',[AboutController::class, 'about']);

Route::get('/login',[AuthController::class, 'login']);
Route::post('/login',[AuthController::class, 'adminLogin']);
Route::get('/logout',[AuthController::class, 'signOut']);

Route::get('/contact',[ContactController::class, 'contact']);

Route::post('/contact',[ContactController::class, 'sendEmail']);


//admin

Route::get('/adminHome',[HomeController::class, 'adminHome']);
Route::post('/adminHome/services',[HomeController::class, 'addServices']);
Route::get('/adminHome/services/delete/{id?}',[HomeController::class, 'deleteServices']);
Route::get('/adminHome/services/edit/{id?}',[HomeController::class, 'editServices']);
Route::post('/adminHome/services/edit/{id?}',[HomeController::class, 'updateServices']);
Route::post('/adminHome/fbpost',[HomeController::class, 'addFBPost']);
Route::get('/adminHome/fbpost/delete/{id?}',[HomeController::class, 'deleteFBpost']);
Route::post('/adminHome/carousel',[HomeController::class, 'addCarousel']);
Route::get('/adminHome/carousel/delete/{id?}',[HomeController::class, 'deleteCarousel']);

Route::get('/adminGallery',[GalleryController::class, 'adminGallery']);
Route::get('/adminGallery/delete/{id?}',[GalleryController::class, 'deleteImage']);
Route::get('/adminGallery/edit/{id?}',[GalleryController::class, 'editImageShow']);
Route::post('/adminGallery/edit/{id?}',[GalleryController::class, 'updateImage']);
Route::post('/adminGallery',[GalleryController::class, 'addImage']);


// Route::post('/adminAbout/about',[AboutController::class, 'addParagraph']);
// Route::get('/adminAbout/about/delete/{id?}',[AboutController::class, 'deleteParagraph']);
// Route::get('/adminAbout/about/edit/{id?}',[AboutController::class, 'editParagraph']);
// Route::post('/adminAbout/about/edit/{id?}',[AboutController::class, 'updateParagraph']);
// Route::get('/adminAbout/mvision/edit/{id?}',[AboutController::class, 'editMVision']);
// Route::post('/adminAbout/mvision/edit/{id?}',[AboutController::class, 'updateMVision']);




Route::get('/adminContact',[ContactController::class, 'adminContact']);
Route::post('/adminContact',[ContactController::class, 'addContactDetails']);
Route::get('/adminContact/delete/{id?}',[ContactController::class, 'deleteContactDetails']);
Route::get('/adminContact/edit/{id?}',[ContactController::class, 'editContactDetails']);
Route::post('/adminContact/edit/{id?}',[ContactController::class, 'updateContactDetails']);

Route::get('/adminAbout',[AboutController::class, 'adminAbout']);
Route::post('ckeditor/upload', [AboutController::class, "upload"])->name('ckeditor.image-upload');
Route::post('ckeditorStore', [AboutController::class, "ckeditorStore"]);


Route::get('/messages',[MessageController::class, 'showMessages'])->name('message');
Route::get('/messages/view/{id?}',[MessageController::class, 'openMessage'])->name('openMessage');

