@extends('master')

@section('title')
    <title>About</title>
@endsection

@section('nav')
    
@endsection

@section('content')
     
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                    <a class="nav-link" href="{{ url('/gallery') }}">Gallery</a>
                    <a class="nav-link active" aria-current="page" href="{{ url('/about') }}">About</a>
                    <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                </div>
            </div>
        </div>
    </nav>

    </nav>

    <div class="container my-3 p-3">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4 my-3">About BigBBoy</h1>
                <hr>
            </div>
            <div class="col-sm-12">
                <style>
                    p {
                      text-indent: 50px;
                      text-align: justify;
                    }
                </style>
                @foreach ($abouts as $about)
                {!! html_entity_decode($about->content) !!}
                @endforeach
            </div>
        </div>
    </div>

    


    <style>
        .map-responsive{
        overflow:hidden;
        padding-bottom:56.25%;
        position:relative;
        height:0;
        }
        .map-responsive iframe{
        left:0;
        top:0;
        height:100%;
        width:100%;
        position:absolute;
        }
    </style>

    <div class="container-fluid mt-5">
        <div class="row d-flex justify-content-center">
            <div class="col-sm-12 w-75">
                <div class="map-responsive">
                    <iframe src="https://maps.google.com/maps?q=Cancino%20Store,%20Rizal%20Street,%20Pozorrubio,%20Pangasinan,%20Philippines&t=&z=13&ie=UTF8&iwloc=&output=embed" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    
@endsection