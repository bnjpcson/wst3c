@extends('master')

@section('title')
    <title>Contact</title>
@endsection

@section('content')

    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                    <a class="nav-link" href="{{ url('/gallery') }}">Gallery</a>
                    <a class="nav-link" href="{{ url('/about') }}">About</a>
                    <a class="nav-link active" aria-current="page" href="{{ url('/contact') }}">Contact</a>
                </div>
            </div>
        </div>
    </nav>

    </nav>


    <style>
        table{
            width: 100%;
        }
        tr, td{
            padding: 10px;
        }
    </style>

    <div class="container my-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4 my-3">Contact</h1>
                <hr>
            </div>
            <div class="col-sm-12">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 mx-auto">
                            <table class="table" style="">
                                <tbody>
                                    @foreach ($contacts as $contact)
                                    <tr>
                                        <td class="fw-bold xx">{{ $contact->contact }}</td>
                                        <td>{{ $contact->details }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <style>
                        .row{
                            margin-top: 15px;
                        }
                    </style>

                    <div class="row">
                        <div class="col-sm-8 mx-auto">
                            <form action="/contact" method="post">

                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">

                                        @if(Session::has('message-sent'))
                                        <div class="alert alert-success">
                                            {{ Session::get('message-sent') }}
                                            @php
                                                Session::forget('message-sent');
                                            @endphp
                                        </div>
                                        @endif
                                        
                                    </div>
                                </div>
                                

                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Name" name="name" aria-describedby="nameHelp" value="{{ request()->input('name', old('name')) }}">
                                    </div>
                                    <small id="nameHelp" class="form-text text-danger">
                                        @error('name')
                                            {{ $message }}
                                        @enderror
                                    </small>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Email" name="email" aria-describedby="emailHelp" value="{{ request()->input('email', old('email')) }}">
                                    </div>
                                    <small id="emailHelp" class="form-text text-danger">
                                        @error('email')
                                            {{ $message }}
                                        @enderror
                                    </small>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Subject" name="subject" aria-describedby="subjectHelp" value="{{ request()->input('subject', old('subject')) }}">
                                    </div>
                                    <small id="subjectHelp" class="form-text text-danger">
                                        @error('subject')
                                            {{ $message }}
                                        @enderror
                                    </small>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea class="form-control" placeholder="Message" rows="3" name="message" aria-describedby="messageHelp">{{ request()->input('message', old('message')) }}</textarea>
                                    </div>
                                    <small id="messageHelp" class="form-text text-danger">
                                        @error('message')
                                            {{ $message }}
                                        @enderror
                                    </small>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn bg-txt-color text-white">Send Message</button>
                                    </div>
                                </div>


                                
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>





    
@endsection