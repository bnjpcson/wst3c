@extends('master')

@section('title')
    <title>Gallery</title>
@endsection

@section('content')
    
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                    <a class="nav-link active" aria-current="page" href="{{ url('/gallery') }}">Gallery</a>
                    <a class="nav-link" href="{{ url('/about') }}">About</a>
                    <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                </div>
            </div>
        </div>
    </nav>

    </nav>

    <style>
        .img-fluid{
            object-fit:fill;
        }
    </style>

    <div class="container my-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4">Gallery</h1>
                <hr>
                <div class="container">
                    <div class="row d-flex justify-content-center">

                        @foreach ($contents as $content)
                        <div class="col-md-3 d-flex justify-content-center">
                            <img class="img-fluid img-thumbnail rounded shadow" src="{{ asset("assets/user/gallery/$content->image_path") }}" onclick="onClick(this)" style="height: 300px; width: 250px;" alt="<strong>{{$content->image_title}}</strong><br><br>{{ $content->description }}">
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal01" class="modal bg-dark p-5 text-end" onclick="this.style.display='none'">
        <span class="btn btn-danger p-3 rounded-circle" title="Close Modal Image">×</span>
        <div class="text-center">
            <div class="row d-flex justify-content-center">
                <div class="col-sm-6">
                    <img id="img01" class="img-fluid mw-100 mh-100">
                    <p id="caption" class="mt-5 text-center h4"></p>
                </div>
            </div>
        </div>
    </div>


    <script>
        // Modal Image Gallery
        function onClick(element) {
          document.getElementById("img01").src = element.src;
          document.getElementById("modal01").style.display = "block";
          var captionText = document.getElementById("caption");
          captionText.innerHTML = element.alt;
        }
        
    </script>

@endsection