@extends('master')

@section('title')
    <title>Home</title>
@endsection

@section('content')



    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link active" aria-current="page" href="{{ url('/') }}">Home</a>
                    <a class="nav-link" href="{{ url('/gallery') }}">Gallery</a>
                    <a class="nav-link" href="{{ url('/about') }}">About</a>
                    <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                </div>
            </div>
        </div>
    </nav>


    

    </nav>


    <style>
        @import url(https://fonts.googleapis.com/css?family=Open+Sans:600);

        /* body {
        font-family: 'Open Sans', sans-serif;
        font-weight: 600;
        font-size: 40px;
        } */

        /* .text {
        position: absolute;
        width: 450px;
        left: 50%;
        margin-left: -225px;
        height: 40px;
        top: 50%;
        margin-top: -20px;
        } */

        /* p {
        display: inline-block;
        vertical-align: top;
        margin: 0;
        } */

        .word {
            position: absolute;
            width: auto;
            opacity: 0;
        }

        .letter {
            color: #FF7F04;
            display: inline-block;
            position: relative;
            transform: translateZ(25px);
            transform-origin: 50% 50% 25px;
        }

        .letter.out {
            transform: rotateX(90deg);
            transition: transform 0.32s cubic-bezier(0.55, 0.055, 0.675, 0.19);
        }

        .letter.behind {
            transform: rotateX(-90deg);
        }

        .letter.in {
            transform: rotateX(0deg);
            transition: transform 0.38s cubic-bezier(0.175, 0.885, 0.32, 1.275);
        }
    </style>




    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-7 my-2">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        @foreach ($carousels as $carousel)
                        
                        
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{$loop->index}}" 
                                class='@if($loop->index == 0) {{"active"}}@endif'
                                aria-current="true" aria-label="Slide {{($loop->index + 1)}}"></button>

                        @endforeach
                    </div>
                    <div class="carousel-inner">
                        @foreach ($carousels as $carousel)
                        <div class="carousel-item
                        @if ($loop->index == 0)
                            {{"active"}}
                        @endif
                        ">
                            <div class="row d-flex justify-content-center">
                                <img src="{{ asset("assets/user/carousel/$carousel->image")}}" class="img-fluid w-75" alt="Profile">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            
            <div class="col-xl-4 my-5 border-start p-5">
                <h1 class="display-4">Motorcycle</h1>
                <div>
                    <h1 class="display-4 word Parts">Parts</h1>
                    <h1 class="display-4 word Accessories">Accessories</h1>
                </div>
                <h1 class="display-4" style="margin-top: 80px;">Shop</h1>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-black45 mt-5">
        <div class="row pt-5">
            <div class="col-12">
                <h1 class="text-center txt-color display-4">Services</h1>
                <hr class="mx-5">
                <div class="row d-flex justify-content-center px-5">
                    
                    @foreach ($services as $service)
                                
                    <div class="col-xl-3 text-center p-3 bg-secondary rounded m-5">
                        <div class="">
                            <img class="img-fluid w-50" src="{{ asset("assets/user/icons/$service->icon") }}"  alt="Icon">
                        </div>
                        
                        {{-- <i class="fa {{$service->icon}} fa-5x p-5 bg-secondary rounded-circle" aria-hidden="true"></i> --}}
                        <h3 class="mt-3 txt-color">{{$service->title}}</h3>
                        <p class="text-white" style="text-align: justify">{{$service->description}}</p>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>

    <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml      : true,
            version    : 'v14.0'
          });
        }; 
    </script>

    <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>

    <style>
        .map-responsive{
        overflow:hidden;
        padding-bottom:75%;
        position:relative;
        height:0;
        }
        .fb-post{
        left:0;
        top:0;
        height:100%;
        width:100%;
        position:absolute;
        }
    </style>
    
    <div class="container-fluid p-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4">Facebook Posts</h1>
                <hr>
                <div class="container">
                    <div class="row">
                        @foreach ($posts as $post)
                        <div class="col-xl-4 mt-5">
                            <div class="map-responsive">
                                <div 
                                class="fb-post" 
                                data-href="{{$post->post_url}}" 
                                data-width="500">
                                </div>
                            </div>
                        </div>
                         @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>

    


    

    
<script>
    var words = document.getElementsByClassName('word');
    var wordArray = [];
    var currentWord = 0;

    words[currentWord].style.opacity = 1;
    for (var i = 0; i < words.length; i++) {
    splitLetters(words[i]);
    }

    function changeWord() {
    var cw = wordArray[currentWord];
    var nw = currentWord == words.length-1 ? wordArray[0] : wordArray[currentWord+1];
    for (var i = 0; i < cw.length; i++) {
        animateLetterOut(cw, i);
    }
    
    for (var i = 0; i < nw.length; i++) {
        nw[i].className = 'letter behind';
        nw[0].parentElement.style.opacity = 1;
        animateLetterIn(nw, i);
    }
    
    currentWord = (currentWord == wordArray.length-1) ? 0 : currentWord+1;
    }

    function animateLetterOut(cw, i) {
    setTimeout(function() {
        cw[i].className = 'letter out';
    }, i*80);
    }

    function animateLetterIn(nw, i) {
    setTimeout(function() {
        nw[i].className = 'letter in';
    }, 340+(i*80));
    }

    function splitLetters(word) {
    var content = word.innerHTML;
    word.innerHTML = '';
    var letters = [];
    for (var i = 0; i < content.length; i++) {
        var letter = document.createElement('span');
        letter.className = 'letter';
        letter.innerHTML = content.charAt(i);
        word.appendChild(letter);
        letters.push(letter);
    }
    
    wordArray.push(letters);
    }

    changeWord();
    setInterval(changeWord, 4000);
</script>


@endsection

