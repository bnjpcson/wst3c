@extends('master')

@section('title')
    <title>Admin | Home</title>
@endsection

@section('content')



    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" aria-current="page" href="{{ url('/adminHome') }}">Home</a>
                    <a class="nav-link" href="{{ url('/adminGallery') }}">Gallery</a>
                    <a class="nav-link" href="{{ url('/adminAbout') }}">About</a>
                    <a class="nav-link" href="{{ url('/adminContact') }}">Contact</a>

                    <div class="nav-link">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i> Admin
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="{{route('message')}}"><i class="fa fa-envelope text-secondary" aria-hidden="true"></i> Messages</a></li>
                                <li><a class="dropdown-item" href="/logout"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </nav>

    <script>
        $(function(){
            $("#messagesTable").DataTable({
                "ordering": false
            });
            $('#showMessageModal').modal("show",{
            backdrop: 'static',
            keyboard: false
            });
        });
    </script>

      <!-- Modal -->
      <div class="modal fade" id="showMessageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            @foreach ($message as $_message)
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Message from {{$_message->name}}</h5>
            </div>
            <div class="modal-body">
            
                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Name</label>
                        <input type="text" class="form-control" name="name" value="{{$_message->name}}" readonly>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Email</label>
                        <input type="text" class="form-control" name="email" value="{{$_message->email}}" readonly>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Subject</label>
                        <input type="text" class="form-control" name="subject" value="{{$_message->subject}}" readonly>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Message</label>
                        <textarea class="form-control" name="message" id="" cols="30" rows="10" readonly>{{$_message->message}}</textarea>
                    </div>
                </div>


            
            </div>
            <div class="modal-footer">
            <a href="/messages" class="btn btn-secondary">Close</a>
            </div>
        
        </div>
        @endforeach
        </div>
    </div>

   


    <div class="container my-5 bg-secondary p-5">
        <div class="row">
            <div class="col-sm-12">
                <table id="messagesTable" class="table p-3">
                    <thead>
                        <tr class="table-dark text-white">
                            <th>Nameasdasd</th>
                            <th>Subject</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($messages as $message)
                    <tr>
                        <td class="text-dark w-50">
                            {{$message->name}}
                        </td>
                        <td class="text-dark w-50">
                            {{$message->subject}}
                        </td>
                        <td>
                            <div class="text-center">
                                <a href="/messages/view/{{$message->id}}" class="ms-2" > <i class="fa fa-eye fa-2x text-primary" aria-hidden="true"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    


    



@endsection

