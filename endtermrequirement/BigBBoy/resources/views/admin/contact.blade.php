@extends('master')

@section('title')
    <title>Admin | Contact</title>
@endsection

@section('content')

    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/adminHome') }}">Home</a>
                    <a class="nav-link" href="{{ url('/adminGallery') }}">Gallery</a>
                    <a class="nav-link" href="{{ url('/adminAbout') }}">About</a>
                    <a class="nav-link active" aria-current="page" href="{{ url('/adminContact') }}">Contact</a>
                    
                    <div class="nav-link">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i> Admin
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="{{route('message')}}"><i class="fa fa-envelope text-secondary" aria-hidden="true"></i> Messages</a></li>
                                <li><a class="dropdown-item" href="/logout"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    </nav>


    <style>
        table{
            width: 100%;
        }
        tr, td{
            padding: 10px;
        }
    </style>

    <script>
        $(function(){
            $("#contactTable").DataTable({
                paging: false,
                info: false,
            }
            );
        });
    </script>

       <!-- Modal -->
       <div class="modal fade" id="addContactDetailsModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Add Contact Details</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <form action="/adminContact" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Contact<span class="text-danger">*</span></label>
                        <input class="form-control" type="text" name="contact">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Details<span class="text-danger">*</span></label>
                        <input class="form-control" type="text" name="details">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>

        </form>
        </div>
        </div>
    </div>



    <div class="container mt-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4 my-3">Contact</h1>
                <hr>
                <div class="container">
                    @if(Session::has('success-contact'))
                    <div class="alert alert-success">
                        {{ Session::get('success-contact') }}
                        @php
                            Session::forget('success-contact');
                        @endphp
                    </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-sm-12 text-end">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addContactDetailsModal">
                            Add Contact Details
                        </button>
                    </div>
                </div>

                <div class="row bg-secondary p-3 rounded my-2">
                    <table id="contactTable" class="table">
                        <thead>
                          <tr class="table-dark text-white text-center">
                            <th>ID</th>
                            <th>Contact</th>
                            <th>Details</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($contacts as $contact)
                        <tr>
                            <td class="text-dark">{{$contact->id}}</td>
                            <td class="text-dark">{{$contact->contact}}</td>
                            <td class="text-dark">{{$contact->details}}</td>
                            <td>
                                <div class="text-center">
                                    <a href="/adminContact/edit/{{$contact->id}}"> <i class="fa fa-edit fa-2x text-warning" aria-hidden="true"></i></a>
                                    <a href="/adminContact/delete/{{$contact->id}}" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 mx-auto">
                            <table class="table" style="">
                                <tbody>
                                    @foreach ($contacts as $contact)
                                    <tr>
                                        <td class="fw-bold xx">{{ $contact->contact }}</td>
                                        <td>{{ $contact->details }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <style>
                        .row{
                            margin-top: 15px;
                        }
                    </style>

                    <div class="row">
                        <div class="col-sm-8 mx-auto">
                            <form action="/contact" method="post">

                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">

                                        @if(Session::has('message-sent'))
                                        <div class="alert alert-success">
                                            {{ Session::get('message-sent') }}
                                            @php
                                                Session::forget('message-sent');
                                            @endphp
                                        </div>
                                        @endif
                                        
                                    </div>
                                </div>
                                

                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Name" name="name" aria-describedby="nameHelp" value="{{ request()->input('name', old('name')) }}">
                                    </div>
                                    <small id="nameHelp" class="form-text text-danger">
                                        @error('name')
                                            {{ $message }}
                                        @enderror
                                    </small>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Email" name="email" aria-describedby="emailHelp" value="{{ request()->input('email', old('email')) }}">
                                    </div>
                                    <small id="emailHelp" class="form-text text-danger">
                                        @error('email')
                                            {{ $message }}
                                        @enderror
                                    </small>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder="Subject" name="subject" aria-describedby="subjectHelp" value="{{ request()->input('subject', old('subject')) }}">
                                    </div>
                                    <small id="subjectHelp" class="form-text text-danger">
                                        @error('subject')
                                            {{ $message }}
                                        @enderror
                                    </small>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea class="form-control" placeholder="Message" rows="3" name="message" aria-describedby="messageHelp">{{ request()->input('message', old('message')) }}</textarea>
                                    </div>
                                    <small id="messageHelp" class="form-text text-danger">
                                        @error('message')
                                            {{ $message }}
                                        @enderror
                                    </small>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn bg-txt-color text-white" disabled>Send Message</button>
                                    </div>
                                </div>


                                
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>





    
@endsection