@extends('master')

@section('title')
    <title>Admin | Gallery</title>
@endsection

@section('content')
    
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/adminHome') }}">Home</a>
                    <a class="nav-link active" aria-current="page" href="{{ url('/adminGallery') }}">Gallery</a>
                    <a class="nav-link" href="{{ url('/adminAbout') }}">About</a>
                    <a class="nav-link" href="{{ url('/adminContact') }}">Contact</a>

                    <div class="nav-link">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i> Admin
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="{{route('message')}}"><i class="fa fa-envelope text-secondary" aria-hidden="true"></i> Messages</a></li>
                                <li><a class="dropdown-item" href="/logout"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    </nav>

    <style>
        .img-fluid{
            object-fit:fill;
        }
    </style>

    <script>
        $(function(){
            $("#galleryTable").DataTable({
                paging: false,
                info: false,
            });

            $('#addImageModal').modal("show",{
            backdrop: 'static',
            keyboard: false
            });
        });
    </script>

      <!-- Modal -->
    <div class="modal fade" id="addImageModal" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Edit Image</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                @foreach ($contents as $content)
            <form action="/adminGallery/edit/{{$content->gallery_id}}" method="post" enctype="multipart/form-data">
                @csrf

               
                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Title<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="title" required value="{{$content->image_title}}">
                    </div>
                </div>


                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Description<span class="text-danger">*</span></label>
                        <textarea class="form-control" rows="3" required name="desc">{{$content->description}}</textarea>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Image<span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="image" accept="image/png, image/gif, image/jpeg" required>
                    </div>
                </div>
           


            
            </div>
            <div class="modal-footer">
            <a href="/adminGallery" class="btn btn-secondary">Cancel</a>
            <button type="submit" class="btn btn-primary">Update</button>
            </div>

        </form>
        @endforeach
        </div>
        </div>
    </div>

    <div class="container p-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4">Gallery</h1>
                <hr>

                <div class="container">
                    @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                        @php
                            Session::forget('success');
                        @endphp
                    </div>
                    @endif
                </div>

                <div class="container my-5">
                    <div class="row">
                        <div class="col-sm-12 text-end">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addImageModal">
                                Add Image
                            </button>
                        </div>
                    </div>

                    <div class="row bg-secondary p-3 rounded my-2">
                        <table id="galleryTable" class="table">
                            <thead>
                              <tr class="table-dark text-white text-center">
                                <th>Image</th>
                                <th>Image Title</th>
                                <th>Description</th>
                                <th>Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach ($contents as $content)
                            <tr>
                                <td class="text-dark">
                                    <img style="width: 100px;" class="img-fluid" src="{{ asset("assets/user/gallery/$content->image_path") }}" alt="{{$content->description}}">
                                </td>
                                <td class="text-dark">{{$content->image_title}}</td>
                                <td class="text-dark">{{$content->description}}</td>
                                <td>
                                    <div class="text-center">
                                        <a href="/adminGallery/edit/{{$content->gallery_id}}"> <i class="fa fa-edit fa-2x text-warning" aria-hidden="true"></i></a>
                                        <a href="/adminGallery/delete/{{$content->gallery_id}}" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
                <div class="container">

                    
                    
                    <div class="row d-flex justify-content-center">

                        @foreach ($contents as $content)
                        <div class="col-md-3 d-flex justify-content-center">
                            <img class="img-fluid rounded shadow" src="{{ asset("assets/user/gallery/$content->image_path") }}" onclick="onClick(this)" style="height: 300px; width: 250px;" alt="<strong>{{$content->image_title}}</strong><br><br>{{ $content->description }}">
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal01" class="modal bg-dark p-5 text-end" onclick="this.style.display='none'">
        <span class="btn btn-danger p-3 rounded-circle" title="Close Modal Image">×</span>
        <div class="text-center">
            <div class="row d-flex justify-content-center">
                <div class="col-sm-6">
                    <img id="img01" class="img-fluid mw-100 mh-100">
                    <p id="caption" class="mt-5 text-center h4"></p>
                </div>
            </div>
        </div>
    </div>


    <script>
        // Modal Image Gallery
        function onClick(element) {
          document.getElementById("img01").src = element.src;
          document.getElementById("modal01").style.display = "block";
          var captionText = document.getElementById("caption");
          captionText.innerHTML = element.alt;
        }
        
    </script>

@endsection