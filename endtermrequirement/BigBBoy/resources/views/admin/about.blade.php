@extends('master')

@section('title')
    <title>Admin | About</title>
@endsection

@section('nav')
    
@endsection

@section('content')
     
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/adminHome') }}">Home</a>
                    <a class="nav-link" href="{{ url('/adminGallery') }}">Gallery</a>
                    <a class="nav-link active" aria-current="page" href="{{ url('/adminAbout') }}">About</a>
                    <a class="nav-link" href="{{ url('/adminContact') }}">Contact</a>

                    <div class="nav-link">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i> Admin
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="{{route('message')}}"><i class="fa fa-envelope text-secondary" aria-hidden="true"></i> Messages</a></li>
                                <li><a class="dropdown-item" href="/logout"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <script>
        $(function(){
            $("#aboutTable").DataTable({
                paging: false,
                info: false,
            }
            );
            $("#mvisionTable").DataTable({
                paging: false,
                info: false,
            }
            );
        });
    </script>


    <div class="container p-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4 my-3">About BigBBoy</h1>
                <hr>
                <div class="container">
                    @if(Session::has('success-about'))
                    <div class="alert alert-success">
                        {{ Session::get('success-about') }}
                        @php
                            Session::forget('success-about');
                        @endphp
                    </div>
                    @endif
                </div>
                <div class="container">
                    <div class="row">
                    <div class="col-md-7 offset-3 mt-4">
                        <div class="card-body">
                            <form method="post" action="ckeditorStore" enctype="multipart/form-data">
                                @csrf

                                <div class="row mb-3">
                                    <div class="col-sm-12 mx-auto">
                                        <div class="form-group">
                                            <textarea class="ckeditor form-control" name="content">
                                                @foreach ($abouts as $about)
                                                {{ $about->content }}
                                                @endforeach
                                            {{-- @foreach ($abouts as $about)
                                            <p>{{ $about->paragraph }}</p>
                                            @endforeach --}}
                                            </textarea>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-12 mx-auto">
                                        <input class="btn btn-primary" type="submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <style>
                            p {
                              text-indent: 50px;
                              text-align: justify;
                            }
                        </style>
                         @foreach ($abouts as $about)
                         {!! html_entity_decode($about->content) !!}
                         @endforeach
                    </div>
                </div>
            </div>
            
        </div>
    </div>


    <style>
        .map-responsive{
        overflow:hidden;
        padding-bottom:56.25%;
        position:relative;
        height:0;
        }
        .map-responsive iframe{
        left:0;
        top:0;
        height:100%;
        width:100%;
        position:absolute;
        }
    </style>

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-sm-12 w-75">
                        <div class="map-responsive">
                            <iframe src="https://maps.google.com/maps?q=Cancino%20Store,%20Rizal%20Street,%20Pozorrubio,%20Pangasinan,%20Philippines&t=&z=13&ie=UTF8&iwloc=&output=embed" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
        $('.ckeditor').ckeditor();
        });
    </script>

    <script type="text/javascript">
        CKEDITOR.replace('content', {
        filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
        });
    </script>

    
@endsection