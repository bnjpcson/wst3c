@extends('master')

@section('title')
    <title>Admin | Home</title>
@endsection

@section('content')



    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link active" aria-current="page" href="{{ url('/adminHome') }}">Home</a>
                    <a class="nav-link" href="{{ url('/adminGallery') }}">Gallery</a>
                    <a class="nav-link" href="{{ url('/adminAbout') }}">About</a>
                    <a class="nav-link" href="{{ url('/adminContact') }}">Contact</a>

                    <div class="nav-link">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i> Admin
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="{{route('message')}}"><i class="fa fa-envelope text-secondary" aria-hidden="true"></i> Messages</a></li>
                                <li><a class="dropdown-item" href="/logout"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </nav>


    

    </nav>


    <style>
        @import url(https://fonts.googleapis.com/css?family=Open+Sans:600);

        .word {
            position: absolute;
            width: auto;
            opacity: 0;
        
        }

        .letter {
            color: #FF7F04;
            display: inline-block;
            position: relative;
            float: left;
            transform: translateZ(25px);
            transform-origin: 50% 50% 25px;
        }

        .letter.out {
            transform: rotateX(90deg);
            transition: transform 0.32s cubic-bezier(0.55, 0.055, 0.675, 0.19);
        }

        .letter.behind {
            transform: rotateX(-90deg);
        }

        .letter.in {
            transform: rotateX(0deg);
            transition: transform 0.38s cubic-bezier(0.175, 0.885, 0.32, 1.275);
        }
    </style>

    <script>
        $(function(){
            $("#servicesTable").DataTable({
                paging: false,
                info: false,
            });
            $('#addServicesModal').modal("show",{
            backdrop: 'static',
            keyboard: false
            });
        });
    </script>


    <div class="container p-5">
        <div class="row">
            <div class="col-sm-12">
                <div class="container my-5">
                    <div class="row align-items-center">
                        <div class="col-sm-5 d-flex justify-content-center">
                            <img src="{{ asset('assets/user/images/Bigbboy.png')}}" alt="Profile" style="width: 400px">
                        </div>
                        <div class="col-sm-7 ps-5 border-start">
                            <h1 class="display-4">Motorcycle</h1>
                            <div>
                                <h1 class="display-4 word Parts">Parts</h1>
                                <h1 class="display-4 word Accessories">Accessories</h1>
                            </div>
                            <h1 class="display-4" style="margin-top: 80px;">Shop</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


      <!-- Modal -->
    <div class="modal fade" id="addServicesModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Edit Services</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            @foreach ($services as $service)
            <form action="/adminHome/services/edit/{{$service->id}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Title<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="title" value="{{$service->title}}" required>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Description<span class="text-danger">*</span></label>
                        <textarea class="form-control" rows="3" required name="desc">{{$service->description}}</textarea>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Icon<span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="icon" required>
                    </div>
                </div>


            
            </div>
            <div class="modal-footer">
            <a href="/adminHome" class="btn btn-secondary">Cancel</a>
            <button type="submit" class="btn btn-primary">Update</button>
            </div>

        </form>
        @endforeach
        </div>
        </div>
    </div>

    


    <div class="container-fluid bg-black45 mt-5">
        <div class="row py-5">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4">Services</h1>
                <hr class="mx-5">
                <div class="container">
                    @if(Session::has('success-services'))
                    <div class="alert alert-success">
                        {{ Session::get('success-services') }}
                        @php
                            Session::forget('success-services');
                        @endphp
                    </div>
                    @endif
                </div>
                <div class="container my-5">
                    <div class="row">
                        <div class="col-sm-12 text-end">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addServicesModal">
                                Add Services
                            </button>
                        </div>
                    </div>

                    <div class="row bg-secondary p-3 rounded my-2">
                        <table id="servicesTable" class="table">
                            <thead>
                              <tr class="table-dark text-white text-center">
                                <th>Icon</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach ($services as $service)
                            <tr>
                                <td class="text-dark">{{$service->icon}}</td>
                                <td class="text-dark">{{$service->title}}</td>
                                <td class="text-dark">{{$service->description}}</td>
                                <td>
                                    <div class="text-center">
                                        <a href="/adminHome/services/edit/{{$service->id}}"> <i class="fa fa-edit fa-2x text-warning" aria-hidden="true"></i></a>
                                        <a href="/adminHome/services/delete/{{$service->id}}" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        @foreach ($services as $service)
                            <div class="col-sm-4 text-center p-5">
                                <i class="fa {{$service->icon}} fa-5x p-5 bg-secondary rounded-circle" aria-hidden="true"></i>
                                <h3 class="mt-3 txt-color">{{$service->title}}</h3>
                                <p>{{$service->description}}</p>
                            </div>
                        @endforeach
                        <div class="col-sm-4 text-center p-5">
                            <i class="fa fa-wrench fa-5x p-5 bg-secondary rounded-circle" aria-hidden="true"></i>
                            <h3 class="mt-3 txt-color">Repair</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit excepturi laboriosam quidem quo minima corrupti hic molestias recusandae facere impedit beatae rem, quaerat, aspernatur quos, explicabo ducimus obcaecati sunt quasi?</p>
                        </div>

                        <div class="col-sm-4 text-center p-5">
                            <i class="fa fa-shopping-cart fa-5x p-5 bg-secondary rounded-circle" aria-hidden="true"></i>
                            <h3 class="mt-3 txt-color">Sell</h3>
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Temporibus quos mollitia maxime vero reiciendis modi, molestias officiis ratione ipsa suscipit tempore quas excepturi perspiciatis dolor sint delectus quasi autem beatae.</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml      : true,
            version    : 'v14.0'
          });
        }; 
    </script>

    <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
    
    <div class="container-fluid p-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4">Latest Posts</h1>
                <hr>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 mt-5">
                            <div 
                            class="fb-post w-50" 
                            data-href="https://www.facebook.com/102031421334719/photos/a.108828347321693/574690534068803/" 
                            data-width="500">
                            </div>
                        </div>
                        <div class="col-xl-4 mt-5">
                            <div 
                            class="fb-post w-50" 
                            data-href="https://www.facebook.com/Bigbboy-Motorcycle-Parts-and-Accessories-Shop-102031421334719/photos/a.108828347321693/574039827467207" 
                            data-width="500">
                            </div>
                        </div>

                        <div class="col-xl-4 mt-5">
                            <div 
                            class="fb-post w-50" 
                            data-href="https://www.facebook.com/Bigbboy-Motorcycle-Parts-and-Accessories-Shop-102031421334719/photos/a.108828347321693/573544310850092" 
                            data-width="500">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    


    

    
<script>
    var words = document.getElementsByClassName('word');
    var wordArray = [];
    var currentWord = 0;

    words[currentWord].style.opacity = 1;
    for (var i = 0; i < words.length; i++) {
    splitLetters(words[i]);
    }

    function changeWord() {
    var cw = wordArray[currentWord];
    var nw = currentWord == words.length-1 ? wordArray[0] : wordArray[currentWord+1];
    for (var i = 0; i < cw.length; i++) {
        animateLetterOut(cw, i);
    }
    
    for (var i = 0; i < nw.length; i++) {
        nw[i].className = 'letter behind';
        nw[0].parentElement.style.opacity = 1;
        animateLetterIn(nw, i);
    }
    
    currentWord = (currentWord == wordArray.length-1) ? 0 : currentWord+1;
    }

    function animateLetterOut(cw, i) {
    setTimeout(function() {
        cw[i].className = 'letter out';
    }, i*80);
    }

    function animateLetterIn(nw, i) {
    setTimeout(function() {
        nw[i].className = 'letter in';
    }, 340+(i*80));
    }

    function splitLetters(word) {
    var content = word.innerHTML;
    word.innerHTML = '';
    var letters = [];
    for (var i = 0; i < content.length; i++) {
        var letter = document.createElement('span');
        letter.className = 'letter';
        letter.innerHTML = content.charAt(i);
        word.appendChild(letter);
        letters.push(letter);
    }
    
    wordArray.push(letters);
    }

    changeWord();
    setInterval(changeWord, 4000);
</script>


@endsection

