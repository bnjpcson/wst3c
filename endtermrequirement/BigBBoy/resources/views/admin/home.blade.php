@extends('master')

@section('title')
    <title>Admin | Home</title>
@endsection

@section('content')



    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link active" aria-current="page" href="{{ url('/adminHome') }}">Home</a>
                    <a class="nav-link" href="{{ url('/adminGallery') }}">Gallery</a>
                    <a class="nav-link" href="{{ url('/adminAbout') }}">About</a>
                    <a class="nav-link" href="{{ url('/adminContact') }}">Contact</a>

                    <div class="nav-link">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i> Admin
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="{{route('message')}}"><i class="fa fa-envelope text-secondary" aria-hidden="true"></i> Messages</a></li>
                                <li><a class="dropdown-item" href="/logout"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </nav>


    

    </nav>


    <style>
        @import url(https://fonts.googleapis.com/css?family=Open+Sans:600);

        .word {
            position: absolute;
            width: auto;
            opacity: 0;
        
        }

        .letter {
            color: #FF7F04;
            display: inline-block;
            position: relative;
            float: left;
            transform: translateZ(25px);
            transform-origin: 50% 50% 25px;
        }

        .letter.out {
            transform: rotateX(90deg);
            transition: transform 0.32s cubic-bezier(0.55, 0.055, 0.675, 0.19);
        }

        .letter.behind {
            transform: rotateX(-90deg);
        }

        .letter.in {
            transform: rotateX(0deg);
            transition: transform 0.38s cubic-bezier(0.175, 0.885, 0.32, 1.275);
        }
    </style>

    <script>
        $(function(){
            $("#servicesTable").DataTable({
                paging: false,
                info: false,
            }
            );
            $("#FBpostTable").DataTable({
                paging: false,
                info: false,
            }
            );
            
            $("#carouselsTable").DataTable({
                paging: false,
                info: false,
            }
            );
        });
    </script>

      <!-- Modal -->
      <div class="modal fade" id="addCarouselModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Add Image Carousel</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <form action="/adminHome/carousel" method="post" enctype="multipart/form-data">
                @csrf
        

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Image<span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>


            
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>

        </form>
        </div>
        </div>
    </div>



    <div class="container my-5">
        @if(Session::has('success-carousel'))
            <div class="alert alert-success">
                {{ Session::get('success-carousel') }}
                @php
                    Session::forget('success-carousel');
                @endphp
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12 text-end">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addCarouselModal">
                    Add Carousel
                </button>
            </div>
        </div>

        <div class="row my-2">
            
                @foreach ($carousels as $carousel)
                <div class="col-xl-4 d-flex justify-content-center">
                    <div class="card w-75 mt-5">
                        <img class="card-img-top" style="display: block;
                            margin: 0 auto;" src="{{ asset("assets/user/carousel/$carousel->image") }}" alt="BigBBoy">
                        <div class="card-body">
                            <div class="text-center">
                                <a href="/adminHome/carousel/delete/{{$carousel->id}}" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    
                </div>
                @endforeach
            
        </div>

        
        <div class="row align-items-center">
            <div class="col-xl-7 my-2">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        @foreach ($carousels as $carousel)
                        
                        
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{$loop->index}}" 
                                class='@if($loop->index == 0) {{"active"}}@endif'
                                aria-current="true" aria-label="Slide {{($loop->index + 1)}}"></button>

                        @endforeach
                        {{-- <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button> --}}
                    </div>
                    <div class="carousel-inner">

                        @foreach ($carousels as $carousel)
                        <div class="carousel-item
                        @if ($loop->index == 0)
                            {{"active"}}
                        @endif
                        ">
                            <div class="row d-flex justify-content-center">
                                <img src="{{ asset("assets/user/carousel/$carousel->image")}}" class="img-fluid w-75" alt="Profile">
                            </div>
                        </div>
                        @endforeach
                        {{-- <div class="carousel-item active">
                            <div class="row d-flex justify-content-center">
                                <img src="{{ asset('assets/user/images/bg1.jpg')}}" class="img-fluid w-75" alt="Profile">
                            </div>
                    
                        </div>
                        <div class="carousel-item">
                            <div class="row d-flex justify-content-center">
                                <img src="{{ asset('assets/user/images/bg.jpg')}}" class="img-fluid w-75" alt="Profile">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row d-flex justify-content-center">
                                <img src="{{ asset('assets/user/images/bg2.jpg')}}" class="img-fluid w-75" alt="Profile">
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            
            <div class="col-xl-4 my-5 border-start p-5">
                <h1 class="display-4">Motorcycle</h1>
                <div>
                    <h1 class="display-4 word Parts">Parts</h1>
                    <h1 class="display-4 word Accessories">Accessories</h1>
                </div>
                <h1 class="display-4" style="margin-top: 80px;">Shop</h1>
            </div>
        </div>
    </div>


      <!-- Modal -->
    <div class="modal fade" id="addServicesModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Add Services</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <form action="/adminHome/services" method="post" enctype="multipart/form-data">
                @csrf
        

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Title<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="title" required>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Description<span class="text-danger">*</span></label>
                        <textarea class="form-control" rows="3" required name="desc"></textarea>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Icon<span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="icon" required>
                    </div>
                </div>


            
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>

        </form>
        </div>
        </div>
    </div>

    


    <div class="container-fluid bg-black45 mt-5 p-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4">Services</h1>
                <hr class="mx-5">
                @if(Session::has('success-services'))
                    <div class="alert alert-success">
                        {{ Session::get('success-services') }}
                        @php
                            Session::forget('success-services');
                        @endphp
                    </div>
                @endif
                <div class="row">
                    <div class="col-sm-12 text-end">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addServicesModal">
                            Add Services
                        </button>
                    </div>
                </div>

                <div class="row bg-secondary rounded my-2 py-5">
                    <table id="servicesTable" class="table" style="display: block;
                    overflow-x: auto;
                    white-space: nowrap;">
                        <thead>
                            <tr class="table-dark text-white text-center">
                            <th>ID</th>
                            <th>Icon</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($services as $service)
                        <tr>
                            <td class="text-dark">{{$service->id}}</td>
                            <td class="text-dark">{{$service->icon}}</td>
                            <td class="text-dark">{{$service->title}}</td>
                            <td class="text-dark">{{$service->description}}</td>
                            <td>
                                <div class="text-center">
                                    <a href="/adminHome/services/edit/{{$service->id}}"> <i class="fa fa-edit fa-2x text-warning" aria-hidden="true"></i></a>
                                    <a href="/adminHome/services/delete/{{$service->id}}" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                </div>
                <div class="container-fluid bg-black45 mt-5">
                    <div class="row py-5">
                        <div class="col-12">
                            <h1 class="text-center txt-color display-4">Services</h1>
                            <hr class="mx-5">
                            <div class="row d-flex justify-content-center">
                                @foreach ($services as $service)
                                
                                <div class="col-xl-3 text-center p-3 bg-secondary rounded m-5">
                                    <div class="">
                                        <img class="img-fluid w-50" src="{{ asset("assets/user/icons/$service->icon") }}"  alt="Icon">
                                    </div>
                                    
                                    {{-- <i class="fa {{$service->icon}} fa-5x p-5 bg-secondary rounded-circle" aria-hidden="true"></i> --}}
                                    <h3 class="mt-3 txt-color">{{$service->title}}</h3>
                                    <p class="text-white" style="text-align: justify">{{$service->description}}</p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </div>


       <!-- Modal -->
    <div class="modal fade" id="addFBPostModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Add Facebook Post</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <form action="/adminHome/fbpost" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Facebook Post URL<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="url" required>
                    </div>
                </div>
   
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
            </div>

        </form>
        </div>
        </div>
    </div>

    <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml      : true,
            version    : 'v14.0'
          });
        }; 
    </script>

    <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
    
    <div class="container mt-5">
        <div class="row">
            <div class="col-sm-12">
                @if(Session::has('success-fbpost'))
                <div class="alert alert-success">
                    {{ Session::get('success-fbpost') }}
                    @php
                        Session::forget('success-fbpost');
                    @endphp
                </div>
                @endif
                <div class="row">
                    <div class="col-sm-12 text-end">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addFBPostModal">
                            Add FB Post
                        </button>
                    </div>
                </div>
                
                <div class="row bg-secondary rounded my-2">
                    <table id="FBpostTable" class="table" style="display: block;
                    overflow-x: auto;
                    white-space: nowrap;">
                        <thead>
                            <tr class="table-dark text-white text-center">
                            <th>ID</th>
                            <th>Facebook Post URL</th>
                            <th>Date Created</th>
                            <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                            <tr>
                                <td class="text-dark">{{$post->id}}</td>
                                <td class="text-dark w-25">{{$post->post_url}}</td>
                                <td class="text-dark">{{$post->created_at}}</td>
                                <td>
                                    <div class="text-center">
                                        {{-- <a href="/adminHome/fbpost/edit/{{$post->id}}"> <i class="fa fa-edit fa-2x text-warning" aria-hidden="true"></i></a> --}}
                                        <a href="/adminHome/fbpost/delete/{{$post->id}}" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                </div>

                
                <style>
                    .map-responsive{
                    overflow:hidden;
                    padding-bottom:75%;
                    position:relative;
                    height:0;
                    }
                    .fb-post{
                    left:0;
                    top:0;
                    height:100%;
                    width:100%;
                    position:absolute;
                    }
                </style>
                
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="text-center txt-color display-4">Facebook Posts</h1>
                            <hr>
                            <div class="container">
                                <div class="row">
                                    @foreach ($posts as $post)
                                    <div class="col-xl-4 mt-5">
                                        <div class="map-responsive">
                                            <div 
                                            class="fb-post" 
                                            data-href="{{$post->post_url}}" 
                                            data-width="500">
                                            </div>
                                        </div>
                                    </div>
                                     @endforeach
                                </div>
                            </div>
            
                        </div>
                    </div>
                </div>
            

            </div>
        </div>
    </div>

    


    

    
<script>
    var words = document.getElementsByClassName('word');
    var wordArray = [];
    var currentWord = 0;

    words[currentWord].style.opacity = 1;
    for (var i = 0; i < words.length; i++) {
    splitLetters(words[i]);
    }

    function changeWord() {
    var cw = wordArray[currentWord];
    var nw = currentWord == words.length-1 ? wordArray[0] : wordArray[currentWord+1];
    for (var i = 0; i < cw.length; i++) {
        animateLetterOut(cw, i);
    }
    
    for (var i = 0; i < nw.length; i++) {
        nw[i].className = 'letter behind';
        nw[0].parentElement.style.opacity = 1;
        animateLetterIn(nw, i);
    }
    
    currentWord = (currentWord == wordArray.length-1) ? 0 : currentWord+1;
    }

    function animateLetterOut(cw, i) {
    setTimeout(function() {
        cw[i].className = 'letter out';
    }, i*80);
    }

    function animateLetterIn(nw, i) {
    setTimeout(function() {
        nw[i].className = 'letter in';
    }, 340+(i*80));
    }

    function splitLetters(word) {
    var content = word.innerHTML;
    word.innerHTML = '';
    var letters = [];
    for (var i = 0; i < content.length; i++) {
        var letter = document.createElement('span');
        letter.className = 'letter';
        letter.innerHTML = content.charAt(i);
        word.appendChild(letter);
        letters.push(letter);
    }
    
    wordArray.push(letters);
    }

    changeWord();
    setInterval(changeWord, 4000);
</script>


@endsection

