@extends('master')

@section('title')
    <title>Admin | About</title>
@endsection

@section('nav')
    
@endsection

@section('content')
     
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/adminHome') }}">Home</a>
                    <a class="nav-link" href="{{ url('/adminGallery') }}">Gallery</a>
                    <a class="nav-link active" aria-current="page" href="{{ url('/adminAbout') }}">About</a>
                    <a class="nav-link" href="{{ url('/adminContact') }}">Contact</a>

                    <div class="nav-link">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i> Admin
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="{{route('message')}}"><i class="fa fa-envelope text-secondary" aria-hidden="true"></i> Messages</a></li>
                                <li><a class="dropdown-item" href="/logout"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <script>
        $(function(){
            $("#aboutTable").DataTable({
                paging: false,
                info: false,
            }
            );
            $("#mvisionTable").DataTable({
                paging: false,
                info: false,
            }
            );
            $('#addParagraphModal').modal("show",{
            backdrop: 'static',
            keyboard: false
            });
        });
    </script>

      <!-- Modal -->
      <div class="modal fade" id="addParagraphModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Edit Paragraph</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            @foreach ($abouts as $about)
            <form action="/adminAbout/edit/{{$about->id}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Paragraph<span class="text-danger">*</span></label>
                        <textarea class="form-control" rows="3" required name="paragraph">{{$about->paragraph}}</textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <a href="/adminAbout" class="btn btn-secondary">Cancel</a>
            <button type="submit" class="btn btn-primary">Update</button>
            </div>

        </form>
        @endforeach
        </div>
        </div>
    </div>


    <div class="container p-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4 my-3">About BigBBoy</h1>
                <hr>
                <div class="container">
                    @if(Session::has('success-about'))
                    <div class="alert alert-success">
                        {{ Session::get('success-about') }}
                        @php
                            Session::forget('success-about');
                        @endphp
                    </div>
                    @endif
                </div>
                <div class="container my-5">
                    <div class="row">
                        <div class="col-sm-12 text-end">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addParagraphModal">
                                Add Paragraph
                            </button>
                        </div>
                    </div>

                    <div class="row bg-secondary p-3 rounded my-2">
                        <table id="aboutTable" class="table">
                            <thead>
                              <tr class="table-dark text-white text-center">
                                <th>ID</th>
                                <th>Paragraph</th>
                                <th>Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach ($abouts as $about)
                            <tr>
                                <td class="text-dark">{{$about->id}}</td>
                                <td class="text-dark">{{$about->paragraph}}</td>
                                <td>
                                    <div class="text-center">
                                        <a href="/adminAbout/about/edit/{{$about->id}}"> <i class="fa fa-edit fa-2x text-warning" aria-hidden="true"></i></a>
                                        <a href="/adminAbout/about/delete/{{$about->id}}" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <style>
                            p {
                              text-indent: 50px;
                              text-align: justify;
                            }
                        </style>
                        @foreach ($abouts as $about)
                        <p>{{ $about->paragraph }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <div class="container-fluid bg-black45 mt-5 p-5">
        <div class="row">
            <div class="col-sm-12 my-5 p-5">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 bg-primary p-5 text-center">
                            <i class="fa fa-bullseye fa-5x mb-3" aria-hidden="true"></i>
                            <h1>Mission</h1>
                            <p class="text-light">Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima ab totam laboriosam, qui quos suscipit odit minus consequuntur velit, accusamus atque architecto praesentium earum deleniti beatae voluptatem. Consequatur, qui corporis!</p>
                        </div>
                        <div class="col-sm-6 bg-success p-5 text-center">
                            <i class="fa fa-eye fa-5x mb-3" aria-hidden="true"></i>
                            <h1>Vision</h1>
                            <p class="text-light">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil quod neque corrupti non quidem? Vel suscipit veritatis culpa, necessitatibus sint ab nemo harum, repudiandae id sapiente magnam! Beatae, expedita.</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <style>
        .map-responsive{
        overflow:hidden;
        padding-bottom:56.25%;
        position:relative;
        height:0;
        }
        .map-responsive iframe{
        left:0;
        top:0;
        height:100%;
        width:100%;
        position:absolute;
        }
    </style>

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center txt-color display-4">Location</h1>
            </div>
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-sm-12 w-75">
                        <div class="map-responsive">
                            <iframe src="https://maps.google.com/maps?q=Cancino%20Store,%20Rizal%20Street,%20Pozorrubio,%20Pangasinan,%20Philippines&t=&z=13&ie=UTF8&iwloc=&output=embed" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    
@endsection