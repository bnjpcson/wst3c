@extends('master')

@section('title')
    <title>Admin | Login</title>
@endsection

@section('nav')
    
@endsection

@section('content')

  
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
        </div>
    </nav>
     
    <main class="login-form my-5 p-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="card bg-light shadow rounded">
                        <h3 class="card-header text-center text-dark">Login</h3>
                        <div class="card-body p-4">

                            <div class="row align-items-center">
                                <div class="col-sm-12 d-flex justify-content-center">
                                    <img src="{{ asset('assets/user/images/Bigbboy.png')}}" alt="Profile" style="width: 150px">
                                </div>
                            </div>

                            <form method="POST" action="/login">
                                @csrf

                                <div class="row">
                                    <div class="col-sm-12">
                                
                                      
                                        @if ($errors->has('errorLogin'))
                                        <div class = "alert alert-danger">
                                            {{ $errors->first('errorLogin') }}
                                        </div>
                                        @endif
                         
                                    </div>
                                </div>

                                <div class="form-group mb-3">
                                    <input type="text" placeholder="Username" id="username" class="form-control" name="username" value="{{ request()->input('username', old('username')) }}">
                                    @if ($errors->has('username'))
                                    <span class="text-danger">{{ $errors->first('username') }}</span>
                                    @endif
                                </div>
    
                                <div class="form-group mb-3">
                                    <input type="password" placeholder="Password" id="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
    
                                <div class="d-grid mx-auto">
                                    <button type="submit" class="btn bg-txt-color text-white">Login</button>
                                </div>
                            </form>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
@endsection