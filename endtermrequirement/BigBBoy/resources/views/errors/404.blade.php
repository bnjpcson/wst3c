@extends('master')

@section('title')
    <title>About</title>
@endsection

@section('nav')
    
@endsection

@section('content')
     
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark py-0 bg-black45 ">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                <img src="{{ asset("assets/user/images/logo.png") }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                    <a class="nav-link" href="{{ url('/gallery') }}">Gallery</a>
                    <a class="nav-link" href="{{ url('/about') }}">About</a>
                    <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                </div>
            </div>
        </div>
    </nav>

    </nav>

    <div class="container my-5" style="height: 520px;">
        <div class="row">
            <div class="col-sm-12">
                

                <div class = "alert alert-danger">
                    <h1 class="text-danger text-center my-5">ERROR 404: Page Not Found.</h1>
                    <div class="text-center my-5"><a href="/" class="btn btn-primary text-center">Go to home</a></div>
                    
                </div>
            </div>
        </div>
        
    </div>
@endsection


{{-- 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ERROR 404</title>

    <!-- Bootstrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap 5 -->

    <!-- In-Line CSS -->
    <style>
        .form-label{
            font-size: 20px;
            font-weight: bold;
        }
        .row{
            margin-top: 20px;
        }
    </style>

</head>
<body>
    
</body>
</html> --}}