<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    //

    public function gallery(Request $request){

        $contents = Gallery::all();

        if ($request->session()->has('admin')) {
            //
            // return redirect('home');

            return redirect('/adminGallery')->with(['contents' => $contents]);
        }else{
            return view('user.gallery', ['contents' => $contents]);
        }
    }

    public function adminGallery(Request $request){

        $contents = Gallery::all();

        if ($request->session()->has('admin')) {
            //
            // return redirect('home');
            return view('admin.gallery', ['contents' => $contents]);
        }else{
            return redirect('/gallery')->with(['contents' => $contents]);
        }
    }

    public function addImage(Request $request){

        $request->validate([
            'title' => 'required',
            'image' => 'required',
            'desc' => 'required'
        ]);

        $title = $request->input('title');
        $image = $request->input('image');
        $desc = $request->input('desc');

        if($request->hasFile('image')){
            $originName = $request->file('image')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('image')->move(public_path('assets/user/gallery'), $fileName);
        }

        $insert = [
            "image_title" => $title,
            "image_path" => $fileName,
            "description" => $desc,
        ];

        Gallery::create($insert);

        return redirect('/adminGallery')->with("success", "Image uploaded successfuly!");

    }

    public function deleteImage(Request $request) {

    
        // $image = Gallery::find($request->id);
        $images = Gallery::where([
            'gallery_id' => $request->id
        ])->get();

        $img_path = "";
        foreach($images as $image){
            $img_path = $image->image_path;
        }

        unlink("assets/user/gallery/".$img_path);


        Gallery::where("gallery_id", $request->id)->delete();

        return redirect('/adminGallery')->with("success", "Image deleted successfully.");

    }

    
    public function editImageShow(Request $request) {

        $images = Gallery::where([
            'gallery_id' => $request->id
        ])->get();

        if(count($images)>0){
            if ($request->session()->has('admin')) {
                //
                return view('admin.editGallery', ['contents' => $images]);
                
                
            }else{
                return redirect('/gallery');
            }
        }else{
            return redirect('/adminGallery');
        }

        
        
    }

    
    public function updateImage(Request $request) {

        
        $request->validate([
            'title' => 'required',
            'image' => 'required',
            'desc' => 'required'
        ]);

        $images = Gallery::where([
            'gallery_id' => $request->id
        ])->get();

        $img_path = "";
        foreach($images as $image){
            $img_path = $image->image_path;
        }

        unlink("assets/user/gallery/".$img_path);


        $title = $request->input('title');
        $imagex = $request->input('image');
        $desc = $request->input('desc');

        if($request->hasFile('image')){
            $originName = $request->file('image')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('image')->move(public_path('assets/user/gallery'), $fileName);
        }

        $update = [
            "image_title" => $title,
            "image_path" => $fileName,
            "description" => $desc,
        ];


        Gallery::where("gallery_id", $request->id)->update($update);

        return redirect('/adminGallery')->with("success", "Image updated successfully.");

    }


}
