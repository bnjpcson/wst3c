<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use App\Models\FBpost;
use App\Models\Services;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    public function home(Request $request){

        $services = Services::orderBy('id', 'ASC')->get();
        $posts = FBpost::orderBy('id', 'DESC')->get();
        $carousels = Carousel::all();

        if ($request->session()->has('admin')) {
            //
            // return redirect('home');

            $admin = session('admin');

            return redirect('/adminHome')->with(['services' => $services, 'posts' => $posts, 'carousels' => $carousels]);
        }else{
            return view('user.home', ['services' => $services, 'posts' => $posts, 'carousels' => $carousels]);
        }
    }

    public function adminHome(Request $request){

        $services = Services::orderBy('id', 'ASC')->get();
        
        $posts = FBpost::orderBy('id', 'DESC')->get();

        $carousels = Carousel::all();

        if ($request->session()->has('admin')) {
            //
            // return redirect('home');
            return view('admin.home', ['services' => $services, 'posts' => $posts, 'carousels' => $carousels]);
        }else{
            return redirect('/')->with(['services' => $services, 'posts' => $posts, 'carousels' => $carousels]);
        }
    }

    public function addServices(Request $request){

        $request->validate([
            'title' => 'required',
            'icon' => 'required',
            'desc' => 'required'
        ]);

        $title = $request->input('title');
        $icon = $request->input('icon');
        $desc = $request->input('desc');

        if($request->hasFile('icon')){
            $originName = $request->file('icon')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('icon')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('icon')->move(public_path('assets/user/icons'), $fileName);
        }

        $insert = [
            "icon" => $fileName,
            "title" => $title,
            "description" => $desc
        ];

        Services::create($insert);

        return redirect('/adminHome')->with("success-services", "Services added successfuly!");


    }

    

    

    public function deleteServices(Request $request) {

    
        // $image = Gallery::find($request->id);
        $services = Services::where([
            'id' => $request->id
        ])->get();

       
        
        if(count($services)>0){
            $img_path = "";
            foreach($services as $service){
                $img_path = $service->icon;
            }
    
            unlink("assets/user/icons/".$img_path);
            Services::where("id", $request->id)->delete();
        }else{
            return redirect('/adminHome');
        }

     

        return redirect('/adminHome')->with("success-services", "Services deleted successfully.");



    }

    public function editServices(Request $request) {


        $services = Services::where([
            'id' => $request->id
        ])->get();

        if(count($services)>0){
            if ($request->session()->has('admin')) {
                //
                return view('admin.editHome', ['services' => $services]);
                
            }else{
                return redirect('/adminHome');
            }
        }else{
            return redirect('/adminHome');
        }
        
    }

    public function updateServices(Request $request) {

        
        $request->validate([
            'title' => 'required',
            'icon' => 'required',
            'desc' => 'required'
        ]);


        $services = Services::where([
            'id' => $request->id
        ])->get();

        if($services){

      

            $title = $request->input('title');
            $icon = $request->input('icon');
            $desc = $request->input('desc');

            $img_path = "";
            foreach($services as $service){
                $img_path = $service->icon;
            }
    
            unlink("assets/user/icons/".$img_path);
            
            if($request->hasFile('icon')){

                $originName = $request->file('icon')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('icon')->getClientOriginalExtension();
                $fileName = $fileName.'_'.time().'.'.$extension;
                $request->file('icon')->move(public_path('assets/user/icons'), $fileName);

                $update = [
                    "icon" => $fileName,
                    "title" => $title,
                    "description" => $desc,
                ];



    
                Services::where("id", $request->id)->update($update);

                
    
                return redirect('/adminHome')->with("success-services", "Services updated successfully.");
            }else{
                return redirect('/adminHome')->with("success-services", "Error updating the record.");
            }

         
            
    

        }else{
            return redirect('/adminHome')->with("success-services", "There's an error updating the record.");
        }


    }

    public function addFBPost(Request $request){

        $request->validate([
            'url' => 'required',
        ]);

        $url = $request->input('url');

        $insert = [
            "post_url" => $url,
        ];

        FBpost::create($insert);

        return redirect('/adminHome')->with("success-fbpost", "Facebook Post added successfuly!");


    }

    public function deleteFBpost(Request $request) {

    
        // $image = Gallery::find($request->id);
        $fbpost = FBpost::where([
            'id' => $request->id
        ])->get();

        
        if(count($fbpost)>0){
            FBpost::where("id", $request->id)->delete();
        }else{
            return redirect('/adminHome');
        }

     

        return redirect('/adminHome')->with("success-fbpost", "Facebook Post deleted successfully.");

    }


    public function addCarousel(Request $request){

        $request->validate([
            'image' => 'required',
        ]);

        $image = $request->input('image');

        if($request->hasFile('image')){
            $originName = $request->file('image')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('image')->move(public_path('assets/user/carousel'), $fileName);

            $insert = [
                "image" => $fileName,
            ];
    
            Carousel::create($insert);
    
            return redirect('/adminHome')->with("success-carousel", "New Carousel added successfuly!");
        }else{
            return redirect('/adminHome')->with("success-carousel", "Failed in adding the image file.");
        }

        


    }

    public function deleteCarousel(Request $request) {

    
        // $image = Gallery::find($request->id);
        $carousels = Carousel::where([
            'id' => $request->id
        ])->get();

       
        
        if(count($carousels)>0){
            $img_path = "";
            foreach($carousels as $carousel){
                $img_path = $carousel->image;
            }
    
            unlink("assets/user/carousel/".$img_path);
            Carousel::where("id", $request->id)->delete();
        }else{
            return redirect('/adminHome');
        }

     

        return redirect('/adminHome')->with("success-carousel", "Image Carousel has been deleted successfully.");



    }

}
