<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Models\ContactDetails;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    //

    public function contact(Request $request){

        $contacts = ContactDetails::all();

        if ($request->session()->has('admin')) {
            //
            // return redirect('home');

            return redirect('adminContact');
        }else{
            return view('user.contact', ['contacts' => $contacts]);
        }
    }

    public function adminContact(Request $request){

        $contacts = ContactDetails::all();

        if ($request->session()->has('admin')) {
            //
            // return redirect('home');
            return view('admin.contact', ['contacts' => $contacts]);
        }else{
            return redirect('contact');
        }
    }

    public function sendEmail(Request $request){

        $name = $request->old('name');
        $email = $request->old('email');
        $subject = $request->old('subject');
        $message = $request->old('message');

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        ]);
    

        $details = [
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'message' => $request->message,
        ];
        
        //Mail::to('benjiepecson1@gmail.com')->send(new ContactMail($details));
        Message::create($details);

        return back()->with('message-sent', 'Your message has been sent successfuly!');
    }

    
    public function addContactDetails(Request $request){



        $request->validate([
            'contact' => 'required',
            'details' => 'required'
        ]);

        $contact = $request->input('contact');
        $details = $request->input('details');


        $insert = [
            "contact" => $contact,
            "details" => $details
        ];

        ContactDetails::create($insert);

        return redirect('adminContact')->with("success-contact", "Contact Details added successfuly!");


    }

    public function deleteContactDetails(Request $request) {

    
        // $image = Gallery::find($request->id);
        $contacts = ContactDetails::where([
            'id' => $request->id
        ])->get();

        
        if(count($contacts)>0){
            ContactDetails::where("id", $request->id)->delete();
        }else{
            return redirect('adminContact');
        }

        return redirect('adminContact')->with("success-contact", "Contact Details deleted successfully.");

    }

    public function editContactDetails(Request $request) {

        $contacts = ContactDetails::where([
            'id' => $request->id
        ])->get();

        if(count($contacts)>0){
            if ($request->session()->has('admin')) {
                //
                return view('admin.editContactDetails', ['contacts' => $contacts]);
                
            }else{
                return redirect('adminContact');
            }
        }else{
            return redirect('adminContact');
        }
        
    }

    public function updateContactDetails(Request $request) {
       
        $request->validate([
            'contact' => 'required',
            'details' => 'required'
        ]);

        $contact = $request->input('contact');
        $details = $request->input('details');


        $update = [
            "contact" => $contact,
            "details" => $details
        ];



        ContactDetails::where("id", $request->id)->update($update);

        return redirect('adminContact')->with("success-contact", "Contact Details updated successfully.");

    }


}
