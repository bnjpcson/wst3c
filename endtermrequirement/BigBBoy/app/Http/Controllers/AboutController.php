<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Ckeditor;
use App\Models\mvision;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    //
    public function about(Request $request){

        $abouts = Ckeditor::all();

        if ($request->session()->has('admin')) {
            //
            // return redirect('home');

            $admin = session('admin');

            return redirect('adminAbout')->with(['abouts' => $abouts]);
        }else{
            return view('user.about', ['abouts' => $abouts]);
        }
    }

    public function adminAbout(Request $request){

        $abouts = Ckeditor::all();

        if ($request->session()->has('admin')) {
            //
            // return redirect('home');

            return view('admin.about', ['abouts' => $abouts]);
        }else{
            return redirect('about')->with(['abouts' => $abouts]);
        }
    }


    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move(public_path('images'), $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName);
            $msg = 'Image successfully uploaded';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }

    
    public function ckeditorStore(Request $request)
    {
        ini_set('memory_limit', '2048M');

        $post = [
        "content" => $request->content
        ];



        Ckeditor::where("id", 1)->update($post);

        return redirect('/adminAbout')->with("success-about", "Details in About Page updated successfully.");
    }

    
    // public function addParagraph(Request $request){

    //     $request->validate([
    //         'paragraph' => 'required',
    //     ]);

    //     $paragraph = $request->input('paragraph');


    //     $insert = [
    //         "paragraph" => $paragraph
    //     ];

    //     About::create($insert);

    //     return redirect('adminAbout')->with("success-about", "Paragraph added successfuly!");


    // }

    // public function deleteParagraph(Request $request) {

    
    //     // $image = Gallery::find($request->id);
    //     $abouts = About::where([
    //         'id' => $request->id
    //     ])->get();

        
    //     if(count($abouts)>0){
    //         About::where("id", $request->id)->delete();
    //     }else{
    //         return redirect('adminAbout');
    //     }

     

    //     return redirect('adminAbout')->with("success-about", "Paragraph deleted successfully.");

    // }

    // public function editParagraph(Request $request) {

    //     $mvisions = mvision::all();

    //     $abouts = About::where([
    //         'id' => $request->id
    //     ])->get();

    //     if(count($abouts)>0){
    //         if ($request->session()->has('admin')) {
    //             //
    //             return view('admin.editAbout', ['abouts' => $abouts, 'mvisions'=>$mvisions]);
                
    //         }else{
    //             return redirect('adminAbout');
    //         }
    //     }else{
    //         return redirect('adminAbout');
    //     }
        
    // }

    // public function updateParagraph(Request $request) {




    //     $request->validate([
    //         'paragraph' => 'required',
    //     ]);

    //     $paragraph = $request->input('paragraph');


    //     $update = [
    //         "paragraph" => $paragraph
    //     ];



    //     About::where("id", $request->id)->update($update);

    //     return redirect('adminAbout')->with("success-about", "Paragraph updated successfully.");

    // }

    

}
