<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    //

    public function login(Request $request)
    {
        if ($request->session()->has('admin')) {
            // return redirect('home');

            $admin = session('admin');

            return redirect('/adminHome');
        }else{
            return view('auth.login');
        }
    }  
       
 
    public function adminLogin(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $username = $request->input('username');
        $password = md5($request->input('password'));
    
       
        // if (Auth::attempt($credentials)) {
        //     // return redirect()->intended('admin.home')->withSuccess('Signed in');
        // }

        //$result = Admin::where('username', '=', 'bigbboy')->get();

        $result = Admin::where([
            'username' => $username,
            'password' => $password,
        ])->get();

        if(count($result)>0){
            $request->session()->put('admin', $username);

            return redirect('/adminHome');
        }else{
            return redirect('login')->withErrors(['errorLogin' => 'Invalid Username or Password']);
        }


        
    }       

    public function signOut(Request $request) {
        $request->session()->pull('admin', 'default');

        $request->session()->forget('admin');

        $request->session()->flush();

        return redirect('/');
    }
}
