<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    //

    public function showMessages(Request $request){

        $messages = Message::orderBy('id', 'DESC')->get();
        
        if ($request->session()->has('admin')) {
            //
            // return redirect('home');
            return view('admin.messages', ['messages' => $messages]);
        }else{
            return redirect('/');
        }
    }

    public function openMessage(Request $request){

        $message = Message::where('id', '=', $request->id)->get();
        $messages = Message::orderBy('id', 'DESC')->get();
        
        if ($request->session()->has('admin')) {
            //
            // return redirect('home');
            return view('admin.openMessage', ['messages' => $messages, 'message' => $message]);
        }else{
            return redirect('/');
        }
    }
}
