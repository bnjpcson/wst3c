<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mvision extends Model
{
    use HasFactory;

    protected $table = 'mvision';
    protected $fillable = [
        'title',
        'description'
    ];
}
