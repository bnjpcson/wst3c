<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FBpost extends Model
{
    use HasFactory;

    protected $table = 'fbposts';
    protected $fillable = [
        'post_url'
    ];
}
