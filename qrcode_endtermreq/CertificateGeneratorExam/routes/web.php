<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccountsController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CertificatesController;
use App\Http\Controllers\GenerateController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\QRCodeController;
use App\Http\Controllers\SeminarController;
use App\Http\Controllers\TemplatesController;
use App\Http\Controllers\VerifyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [VerifyController::class, 'displayVerification']);
Route::post('/', [VerifyController::class, 'verify']);

Route::get('/sign_up', function () {
    return view('sign_up');
});

Route::get('/admin', function () {
    if(session()->has('username')){
        return redirect('/generator');
    }
    else{
        return redirect('/admin');
    }  
});

// Route::get('/admin_home', function () {
//     if(session()->has('username')){
//         return view('admin_home');
//     }
//     else{
//         return redirect('/admin');
//     }     
// });

Route::get('/logout', function () {
    if(session()->has('username')){
        session()->pull('username');
    }
    return redirect('/admin');
})->name('logout');

Route::get('/admin', [AccountsController::class, 'loginAdminForm' ]);
Route::post('/admin', [AccountsController::class, 'validateAdminForm' ]);






Route::get('/seminars', [SeminarController::class, 'displaySeminar'])->name('seminars');
Route::post('/seminars', [SeminarController::class, 'addSeminar']);
Route::get('/seminars/edit/{id}', [SeminarController::class, 'displayEditSeminar']);
Route::post('/seminars/edit/{id}', [SeminarController::class, 'updateSeminar']);
Route::get('/seminars/delete/{id}', [SeminarController::class, 'deleteSeminar']);
Route::get('/seminars/status/{id}', [SeminarController::class, 'updateStatus']);


//Route::get('/qr-code',[QRCodeController::class,'index']);

Route::get('/generator', [AdminController::class, 'displayHome'])->name('generator');
Route::post('/generator', [AdminController::class, 'displayHome']);

Route::get('/templates', [TemplatesController::class, 'displayTemplate'])->name('templates');

Route::post('/templates', [TemplatesController::class, 'previewTemplate']);

Route::get('/templates/save/{seminar_name}/{id}', [TemplatesController::class, 'saveTemplate']);


Route::get('/templates/delete/{id}', [TemplatesController::class, 'deleteTemplate']);

Route::get('/generator/save/{seminar_name}/{awardee}/{token}', [AdminController::class, 'saveCertificate']);

Route::get('/certs', [CertificatesController::class, 'displayCertificates'])->name('certs');

Route::post('/templates/addImage', [TemplatesController::class, 'addBGImage']);


Route::get('/admins', [AdminController::class, 'displayAdmin'])->name('admins');
Route::post('/admins', [AdminController::class, 'adminInsert']);
Route::post('/admins/delete/{id}', [AdminController::class, 'adminDelete']);

Route::post('certs/sendmail/{id}', [MailController::class, 'sendEmail']);

Route::get('certs/mailsent/{id}', [MailController::class, 'displayMailsent']);

