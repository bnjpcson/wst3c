-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2022 at 06:59 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `certs_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `email`, `username`, `password`, `updated_at`, `created_at`) VALUES
(1, 'admin123@gmail.com', 'admin123', '0192023a7bbd73250516f069df18b500', NULL, NULL),
(7, 'benjiepecson@yahoo.com', 'benjie12', 'd74a6470cd5f9ec40f790ef46057cb7c', '2022-06-29 12:11:04', '2022-06-29 12:11:04');

-- --------------------------------------------------------

--
-- Table structure for table `bg_templates`
--

CREATE TABLE `bg_templates` (
  `id` int(11) NOT NULL,
  `temp_name` varchar(255) NOT NULL,
  `imgpath` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bg_templates`
--

INSERT INTO `bg_templates` (`id`, `temp_name`, `imgpath`, `created_at`, `updated_at`) VALUES
(2, 'Template #1', 'assets/templates/5_1656525209.png', '2022-06-29 09:53:29', '2022-06-29 09:53:29'),
(3, 'Template #2', 'assets/templates/9_1656525222.png', '2022-06-29 09:53:42', '2022-06-29 09:53:42'),
(4, 'Template #3', 'assets/templates/8_1656525239.png', '2022-06-29 09:53:59', '2022-06-29 09:53:59'),
(5, 'Template #4', 'assets/templates/template_1656747876.png', '2022-07-01 23:44:36', '2022-07-01 23:44:36'),
(6, 'PSU Template 1', 'assets/templates/template1_1656939862.png', '2022-07-04 05:04:22', '2022-07-04 05:04:22'),
(7, 'PSU Template 2', 'assets/templates/template2_1656939887.png', '2022-07-04 05:04:47', '2022-07-04 05:04:47'),
(8, 'Template #7', 'assets/templates/template2_1656942044.png', '2022-07-04 05:40:44', '2022-07-04 05:40:44');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(11) NOT NULL,
  `seminar_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `awardee` varchar(255) NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `creator` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `seminar_id`, `token`, `awardee`, `img_path`, `creator`, `created_at`, `updated_at`) VALUES
(17, 12, '04a2b0e18dbf96ee431285134e4792a7', 'Benjie Pecson', 'certificates/certificate_2919-17-24.png', 'admin123@gmail.com', '2022-06-29 11:17:24', '2022-06-29 11:17:24'),
(18, 12, 'a5034a6b21432c189687d17f64649350', 'Benjie Pecson', 'certificates/certificate_2919-35-07.png', 'admin123@gmail.com', '2022-06-29 11:35:07', '2022-06-29 11:35:07'),
(19, 13, 'b165fed1584f10b1404094e709f36c9c', 'Katrina Urbano', 'certificates/certificate_2922-05-49.png', 'admin123@gmail.com', '2022-06-29 14:05:49', '2022-06-29 14:05:49'),
(20, 12, '6e69c9d022d91f49268430ab43366bb3', 'Jonel Nagtalon', 'certificates/certificate_2922-09-38.png', 'admin123@gmail.com', '2022-06-29 14:09:38', '2022-06-29 14:09:38'),
(21, 13, '2e7f57fae4303b9b7eb64f31fada5c91', 'Louie Catabay', 'certificates/certificate_2922-31-54.png', 'admin123@gmail.com', '2022-06-29 14:31:54', '2022-06-29 14:31:54'),
(22, 12, '842266d6aa0cde2417fb5c26768c5c51', 'Windel Rodillas', 'certificates/certificate_3005-26-25.png', 'admin123@gmail.com', '2022-06-29 21:26:25', '2022-06-29 21:26:25'),
(23, 13, '3d8e472cf3ba9b5ba527c4c724299133', 'Camila Delos Santos', 'certificates/certificate_3005-27-34.png', 'benjiepecson@yahoo.com', '2022-06-29 21:27:34', '2022-06-29 21:27:34'),
(24, 14, 'c392430bc3205a6ee7e409538184573c', 'July Rosal', 'certificates/certificate_3006-44-14.png', 'benjiepecson@yahoo.com', '2022-06-29 22:44:14', '2022-06-29 22:44:14'),
(25, 15, '11e2afda8f9b1607502f6493d13b3870', 'Rhaemonette', 'certificates/certificate_0110-12-23.png', 'admin123@gmail.com', '2022-07-01 02:12:23', '2022-07-01 02:12:23'),
(27, 12, 'e57381ce365bfcda3184631918810040', 'Rhaemonette', 'certificates/certificate_0110-17-08.png', 'admin123@gmail.com', '2022-07-01 02:17:08', '2022-07-01 02:17:08'),
(28, 23, 'b1691d0160606436f60024d059863947', 'Benjie Pecson', 'certificates/certificate_0413-12-18.png', 'admin123@gmail.com', '2022-07-04 05:12:18', '2022-07-04 05:12:18'),
(29, 23, 'f2c8f21d3949beb670e11d7738f70949', 'Benjie Pecson', 'certificates/certificate_0502-04-35.png', 'admin123@gmail.com', '2022-07-04 18:04:35', '2022-07-04 18:04:35');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cert_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `cert_id`, `email`, `created_at`, `updated_at`) VALUES
(1, 17, 'benjiepecson1@gmail.com', '2022-06-29 14:42:49', '2022-06-29 14:42:49'),
(2, 18, 'benjiepecson@yahoo.com', '2022-06-29 14:43:27', '2022-06-29 14:43:27'),
(3, 19, 'kat@gmail.com', '2022-06-29 14:46:29', '2022-06-29 14:46:29'),
(4, 27, 'rhaemonette@gmail.com', '2022-07-01 02:27:22', '2022-07-01 02:27:22'),
(5, 27, 'rhaemonation@gmail.com', '2022-07-01 23:24:48', '2022-07-01 23:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(17, '2014_10_12_000000_create_users_table', 1),
(18, '2014_10_12_100000_create_password_resets_table', 1),
(19, '2019_08_19_000000_create_failed_jobs_table', 1),
(20, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(21, '2022_06_20_132553_create_seminar_table', 1),
(22, '2022_06_29_204531_create_emails_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seminars`
--

CREATE TABLE `seminars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `edate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `venue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seminars`
--

INSERT INTO `seminars` (`id`, `title`, `sdate`, `edate`, `venue`, `status`, `created_at`, `updated_at`) VALUES
(12, 'Developing Outstanding Leadership Skills.', '2022-06-28', '2022-06-30', 'Urdaneta City, Pangasinan', 1, '2022-06-26 02:20:50', '2022-06-26 02:20:50'),
(13, 'Developing Business Management Skills for Women.', '2022-06-30', '2022-07-01', 'Urdaneta City, Pangasinan', 1, '2022-06-26 02:34:08', '2022-06-26 03:27:11'),
(14, 'How To Make Lumpia', '2022-07-02', '2022-07-02', 'Urdaneta City, Pangasinan', 1, '2022-06-26 04:40:48', '2022-06-29 22:42:35'),
(15, 'Creating Value by Managing the Source of Your Stress', '2022-07-01', '2022-07-01', 'Urdaneta City, Pangasinan', 1, '2022-06-26 08:12:00', '2022-06-26 08:12:00'),
(23, 'Introduction to Laravel Framework', '2022-07-10', '2022-07-16', 'Urdaneta City, Pangasinan', 1, '2022-07-04 05:06:46', '2022-07-04 05:06:46'),
(24, 'Introduction to JQUERY', '2022-07-29', '2022-08-06', 'Binalonan, Pangasinan', 1, '2022-07-04 05:10:28', '2022-07-04 05:10:28'),
(25, 'AWS', '2022-07-09', '2022-07-09', 'Urdaneta City, Pangasinan', 1, '2022-07-04 18:05:43', '2022-07-04 18:05:43');

-- --------------------------------------------------------

--
-- Table structure for table `signature`
--

CREATE TABLE `signature` (
  `signature_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `e_sig` varchar(100) NOT NULL,
  `seminar_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `id` int(11) NOT NULL,
  `seminar_name` text NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `template` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `seminar_name`, `img_path`, `template`, `created_at`, `updated_at`) VALUES
(18, 'Developing Outstanding Leadership Skills.', 'templatesFolder/seminar_2918-44-37.png', 2, '2022-06-29 10:44:37', '2022-06-29 10:44:37'),
(19, 'Developing Business Management Skills for Women.', 'templatesFolder/seminar_2919-03-56.png', 3, '2022-06-29 11:03:56', '2022-06-29 11:03:56'),
(21, 'How To Make Lumpia', 'templatesFolder/seminar_3006-43-54.png', 4, '2022-06-29 22:43:54', '2022-06-29 22:43:54'),
(23, 'Introduction to Laravel Framework', 'templatesFolder/seminar_0413-11-59.png', 6, '2022-07-04 05:11:59', '2022-07-04 05:11:59'),
(24, 'Introduction to JQUERY', 'templatesFolder/seminar_0413-28-18.png', 7, '2022-07-04 05:28:19', '2022-07-04 05:28:19'),
(25, 'AWS', 'templatesFolder/seminar_0502-13-14.png', 8, '2022-07-04 18:13:14', '2022-07-04 18:13:14');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(20) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(15) NOT NULL,
  `password` varchar(12) NOT NULL,
  `event_title` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `bg_templates`
--
ALTER TABLE `bg_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `seminars`
--
ALTER TABLE `seminars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signature`
--
ALTER TABLE `signature`
  ADD PRIMARY KEY (`signature_id`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `bg_templates`
--
ALTER TABLE `bg_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seminars`
--
ALTER TABLE `seminars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `signature`
--
ALTER TABLE `signature`
  MODIFY `signature_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
