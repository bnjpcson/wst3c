@extends('master')

@section('title')
    <title>Admin Log In</title>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link href="/css/A_home.css" rel="stylesheet" type="text/css">

    <!-- Font link -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&family=Open+Sans:wght@300&display=swap" rel="stylesheet">

    <script type="text/javascript">
        $(function() {
            var timeout = 3000; // in miliseconds (3*1000)
            $('.alert').delay(timeout).fadeOut(300);
        });
    </script>

    <style>
        .f1 {
            font-family: 'Montserrat', sans-serif;
        }
        .f2 {
            font-family: 'Open Sans', sans-serif;
        }
    </style>
@endsection

@section('content')
<header class="header">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">

</header>
    <div class="container-fluid">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                       </ul>
                   </div>
        @endif
    </div>
   
<section class="">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center mt-5" >
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card text-white" style="border-radius: 1rem; background-color: #082b54;">
          <div class="card-body p-5 text-center">

            <div class="mb-md-5 mt-md-4">
                <form action="" method="POST">
                {{ csrf_field() }}
                <h2 class="fw-bold mb-2 text-uppercase f1">Sign In</h2>
                <p class="text-white-50 mb-5 f2">Please enter your valid email/username and password.</p>

                <div class="form-floating mb-3">
                  <input type="text" class="form-control f2" id="floatingInput" placeholder="name@example.com" name="username">
                  <label for="floatingInput" class="text-dark">Email/Username</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control f2" id="floatingPassword" placeholder="Password" name="password">
                    <label for="floatingPassword" class="text-dark">Password</label>
                </div>

                <button class="btn btn-outline-light btn-lg px-5 mt-4 f1" type="submit">Login</button>
                </form>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
    @include('sweetalert::alert')
    @stack('javascript-external')
    @stack('javascript-internal')
@endsection