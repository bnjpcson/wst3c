@extends('master')

@section('title')
    <title>Seminars</title>
 
  	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
  	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link href="/css/A_home.css" rel="stylesheet" type="text/css">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


    {{-- Datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    {{-- Datatables --}}

  
	  <title>Seminars</title>

    <style type="text/css">
      .hello{
        font-size: 30px;
      }
      .active::before{
        background: white;
      }
      .active{
        color: #082b54;
      }
    </style>
    @stack('css-external')
    @stack('css-internal')
@endsection

@section('content')


<header class="header" style="height:130px;">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show my-auto">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="seminars/" class="btn topnav__link active">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="templates/" class="btn topnav__link">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="generator/" class="btn topnav__link">Generator</a>
    </li>
    <li class="topnav__item">
      <a href="certs/" class="btn topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="/admins" class="btn topnav__link">Admins</a>
    </li>
    <li class="topnav__item">
      <div class="dropdown">
        <button class="btn topnav__link dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
          My Profile
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
          <li><a class="dropdown-item" href="{{asset('manual/manual.pdf');}}" download>User's Manual</a></li>
          <li><a href="/logout" class="dropdown-item"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
        </ul>
      </div>
    </li>
  </ul>
  <div class="p-1 position-absolute bottom-0 end-0 text-end pe-3">
    <span id='ct6'></span>
  </div>
</header>

<div class="container">
  @if(Session::has('success-seminar'))
  <div class="alert alert-success">
      {{ Session::get('success-seminar') }}
      @php
          Session::forget('success-seminar');
      @endphp
  </div>
  @endif
</div>



<div class="card mb-3 col-sm-6 mx-auto mt-5">
  {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}

  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  @if(Session::has('success-seminar'))
    <div class="alert alert-success">
        {{ Session::get('success-seminar') }}
        @php
            Session::forget('success-seminar');
        @endphp
    </div>
  @endif



  <div class="card-body">
    <div class = "container col-sm-12">
		<form action="/seminars" method = "post" enctype="multipart/form-data">
	        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
	        <div class="form-row">
	            <div class="input-group mb-3">
	                <span class="input-group-text text-dark fw-bold">Title</span>
	                <input type="text" name="title" class="form-control">
	            </div>
	            <div class="input-group">
                <span class="input-group-text text-dark fw-bold">Date</span>
                <input type="date" class="form-control" name="sdate"> 
                <span class="p-2 fw-bold">TO</span>
                <input type="date" class="form-control" name="edate">
                <span aria-describedby="dateHelp"></span>
                
              </div>
              <div id="dateHelp" class="form-text mb-3">The date must be the day after tomorrow</div>
	            <div class="input-group mb-3">
	                <span class="input-group-text text-dark fw-bold">Venue</span>
	                <input type="text" name="venue" class="form-control">
	            </div>


	            <center>
	            	<button type="submit" class="btn btn-block mx-auto text-white" style="background-color: #082b54">Add Seminar</button>
	            </center>
	        </div>
	    </form>
	</div>
  </div>
</div>

<script>
  $(function(){
        $("#seminarsTable").DataTable({
          order: [[0, 'desc']],
        });
    });
</script>

@push('javascript-internal')
<script>
      $(document).ready(function() {
         //Event: delete
         $("form[role='alert']").submit(function(event) {
            event.preventDefault();
            Swal.fire({
            title: $(this).attr('alert-title'),
            text:  $(this).attr('alert-text'),
            icon: 'warning',
            allowOutsideClick: false,
            showCancelButton: true,
            cancelButtonText: $(this).attr('alert-btn-cancel'),
            reverseButtons: true,
            confirmButtonText: $(this).attr('alert-btn-yes'),
         }).then((result) => {
            if (result.isConfirmed) {
               //process ng deleting 
              event.target.submit();
             
            }
         });

         });

      });
      
  </script>
@endpush


<div class="container">
  <div class="row my-5">
    <div class="col-sm-12 mx-auto">
      <table class="table w-100 mx-auto" id="seminarsTable">
        <thead class="bg-dark text-white">
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Date</th>
            <th>Venue</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($seminars as $seminar)
          <tr>
            <td>{{$seminar->id}}</td>
            <td>{{$seminar->title}}</td>
            @if($seminar->sdate == $seminar->edate)
            <td>{{$seminar->sdate}}</td>
            @elseif($seminar->sdate != $seminar->edate)
            <td>{{$seminar->sdate}} to {{$seminar->edate}}</td>
            @endif
            <td>{{$seminar->venue}}</td>
            <td>

              @if ($seminar->status == "1")
              {{-- <a href="/seminars/status/{{$seminar->id}}" class="btn btn-success">{{$seminar->status}}</a> --}}
              <span class="text-success">Active</span>
              @else
              {{-- <button class="btn" style="background-color:gray; color: white;" disabled="">
                {{$seminar->status}}
              </button> --}}
              <span style="color: gray;">Inactive</span>
              @endif
              
            </td>
            <td>
                <form action="/seminars/delete/{{$seminar->id}}" role="alert" method="GET" 
                alert-title="{{trans('seminars.alert.delete.title')}}" alert-text="{{trans('seminars.alert.delete.message.confirm',['title' => $seminar->title])}}"
                alert-btn-cancel="{{trans('seminars.button.cancel.value')}}" alert-btn-yes="{{trans('seminars.button.delete.value')}}">
                  <div class="text-center">
                    <a href="/seminars/edit/{{$seminar->id}}"  class="btn btn-sm btn-warning"
                      role="button"> <i  class="fas fa-edit"></i></a>
                    @csrf
                    <!-- @method('DELETE') -->
                    <button type="submit" class="btn btn-sm btn-danger"> <i class=" fas fa-trash"></i>
                    </button>
                  </div>
                </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@include('sweetalert::alert')

@push('javascript-external')
@endpush

@stack('javascript-external')
@stack('javascript-internal')



@push('css-external')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2-bootstrap4.min.css') }}">
@endpush

@push('javascript-external')
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
@endpush


@endsection
