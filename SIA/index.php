<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Create Account in Bitbucket</title>
</head>

<style>
  table{
    margin: auto;
    border-collapse: collapse;
  }
  td{
    border: 1px solid black;
    padding: 5px;
  }
</style>
<body>
  <table>
    <tr>
      <td>Name</td>
      <td>Benjie Pecson</td>
    </tr>
    <tr>
      <td>Year and Section</td>
      <td>4-C</td>
    </tr>
    <tr>
      <td>Subject</td>
      <td>System Integration and Architecture</td>
    </tr>
    <tr>
      <td>Date and Time</td>
      <td>September 29, 2022 - 2:38 PM</td>
    </tr>
  </table>
</body>
</html>