<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Seminar;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
    //

    public function displayVerification(Request $request){
        if(session()->has('username')){
            return redirect('/generator');
        }
        else{
            $details = [];
            return view('verify')->with(['details'=> $details]);
        }  
    }

    public function verify(Request $request){

        $request->validate([
            'token' => 'required|min:32|max:32',
        ]);

        $_token = $request->input('token');

        $certificates = Certificate::where([
            'token' => $_token
        ])->get();

        if(count($certificates)>0){

            foreach($certificates as $certificate){
                $seminar_id = $certificate->seminar_id;
                $awardee = $certificate->awardee;
                $img_path = $certificate->img_path;
            }

            $seminar = Seminar::where([
                'id' => $seminar_id
            ])->get();

            if(count($seminar)>0){

                foreach($seminar as $seminars){
                    $title = $seminars->title;
                    $date = $seminars->date;
                    $venue = $seminars->venue;
                }

                $details = [
                    "token" => $_token,
                    "name" => $awardee,
                    "seminar" => $title,
                    "venue" => $venue,
                    "date" => $date,
                    "img_path" => $img_path
                ];

                // dd($details);

                return view('verify')->with(['details'=> $details]);

            }else{
                return redirect('/')->withErrors("Seminar not found.");
            }

        }else{
            return redirect('/')->withErrors("Participant not found.");
        }
    }
}
