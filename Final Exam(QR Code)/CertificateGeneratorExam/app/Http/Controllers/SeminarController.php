<?php

namespace App\Http\Controllers;

use App\Models\Seminar;
use Illuminate\Http\Request;

class SeminarController extends Controller
{
    //

    public function displaySeminar(Request $request){
        $seminars = Seminar::all();

        if ($request->session()->has('username')) {

            return view('seminars', ['seminars' => $seminars]);
        }else{
            return redirect('admin');
        }
    }

    public function addSeminar(Request $request){
        $request->validate([
            'title' => 'required',
            'date' => 'required|after:tomorrow',
            'venue' => 'required',
        ]);

        
        $title = $request->input('title');
        $date = $request->input('date');
        $venue = $request->input('venue');


        $insert = [
            "title" => $title,
            "date" => $date,
            "venue" => $venue
        ];

        Seminar::create($insert);

        return redirect('seminars')->with("success-seminar", "Seminar added successfuly!");
    }

    public function displayEditSeminar(Request $request){
        $seminars = Seminar::where('id', '=', $request->id)->get();

        if ($request->session()->has('username')) {

            return view('editseminars', ['seminars' => $seminars]);
        }else{
            return redirect('admin');
        }

    }

    public function updateSeminar(Request $request){
        $request->validate([
            'title' => 'required',
            'date' => 'required|after:tomorrow',
            'venue' => 'required',
        ]);

        $title = $request->input('title');
        $date = $request->input('date');
        $venue = $request->input('venue');


        $update = [
            "title" => $title,
            "date" => $date,
            "venue" => $venue
        ];

        Seminar::where("id", $request->id)->update($update);

        return redirect('seminars')->with("success-seminar", "Seminar updated successfully.");

    }

    public function deleteSeminar(Request $request){

        $seminar = Seminar::where([
            'id' => $request->id
        ])->get();


        if(count($seminar)>0){
            Seminar::where("id", $request->id)->delete();
            return redirect('seminars')->with("success-seminar", "Seminar deleted successfully.");
        }else{
            return redirect('seminars')->withErrors("Error");
        }

    }

    public function updateStatus(Request $request){

        $seminar = Seminar::where([
            'id' => $request->id
        ])->get();

        if(count($seminar)>0){
            $update = [
                "status" => "INACTIVE"
            ];
    
            Seminar::where("id", $request->id)->update($update);
    
            return redirect('seminars')->with("success-seminar", "Seminar updated successfully.");
        }else{
            return redirect('seminars')->withErrors("Error");
        }

        

    }
}
