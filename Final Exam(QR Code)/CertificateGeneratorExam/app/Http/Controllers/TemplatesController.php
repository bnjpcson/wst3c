<?php

namespace App\Http\Controllers;

use App\Models\Seminar;
use App\Models\Template;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class TemplatesController extends Controller
{
    //

    public function displayTemplate(Request $request){
        $templates = Template::all();
        $seminars = Seminar::where([
            ['status', '=', 'ACTIVE']
        ])->get();

        // $seminars = Seminar::all();

        if ($request->session()->has('username')) {

            return view('templates', ['templates' => $templates, 'seminars' => $seminars]);
        }else{
            return redirect('admin');
        }
    }

    public function previewTemplate(Request $request){
        $request->validate([
            'seminar' => 'required|unique:templates,seminar_name',
            'contents' => 'required',
            'logo' => 'required',
            'signame1' => 'required',
            'esig1' => 'required',
            'signame2' => 'required',
            'esig2' => 'required',
        ]);

        $templates = Template::all();
        $seminars = Seminar::where([
            ['status', '=', 'ACTIVE']
        ])->get();

        if ($request->session()->has('username')) {

            return view('templates', ['templates' => $templates, 'seminars' => $seminars]);
        }else{
            return redirect('admin');
        }
    }

    public function saveTemplate(Request $request, $seminar_name){
        //move previews/newpreview to templates folder

        $ldate = date('dH-i-s');

        $filename = "templatesFolder/seminar_".$ldate.".png";

       $insert = [
        "seminar_name" => $seminar_name,
        "img_path" => $filename
       ];




       Template::create($insert);
       File::move(public_path('previews/newpreview.png'), public_path($filename));


       return redirect('templates')->with("success-template", "Template created successfuly!");

    }

    public function deleteTemplate(Request $request, $id){

       $template = Template::where([
        'id' => $request->id
        ])->get();


        if(count($template)>0){

            foreach($template as $templates){
                $img = $templates->img_path;
            }

            unlink($img);
            Template::where("id", $request->id)->delete();
            return redirect('templates')->with("success-template", "Template deleted successfully.");
        }else{
            return redirect('templates')->withErrors("Error");
        }

    }
}
