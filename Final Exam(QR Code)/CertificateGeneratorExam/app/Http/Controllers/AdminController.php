<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Seminar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{
    //
    public function displayHome(Request $request){
        // $seminars = Seminar::all();

        $seminars = Seminar::where([
            ['status', '=', 'ACTIVE']
        ])->get();

        if ($request->session()->has('username')) {
            return view('generator', ['seminars' => $seminars]);
        }else{
            return redirect('admin');
        }
    }

    public function saveCertificate(Request $request, $seminar, $awardee, $token){


        $seminar = Seminar::where([
            'title' => $seminar
        ])->get();

        if(count($seminar)>0){
            //save


            foreach($seminar as $seminars){
                $seminar_id = $seminars->id;
            }

            $ldate = date('dH-i-s');

            $filename = "certificates/certificate_".$ldate.".png";

            $insert = [
                "seminar_id" => $seminar_id,
                "token" => $token,
                "awardee" => $awardee,
                "img_path" => $filename
            ];

            Certificate::create($insert);
            File::move(public_path('previews/newcertificate.png'), public_path($filename));


            return redirect('generator')->with("success-generator","Certificate generated successfully!");
        

        }else{
            return redirect('generator')->withErrors("Seminar doesn't exist.");
        }
    }
}
