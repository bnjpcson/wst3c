<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use Illuminate\Http\Request;

class CertificatesController extends Controller
{
    //
    public function displayCertificates(Request $request){
        $certificates = Certificate::all();

        if ($request->session()->has('username')) {
            return view('certificates', ['certificates' => $certificates]);
        }else{
            return redirect('admin');
        }
    }
}
