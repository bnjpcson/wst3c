<!DOCTYPE html>
<html>
<head>
  	<link rel="icon" href="/assets/fav.png" type="image/x-icon">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
  	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link href="/css/A_home.css" rel="stylesheet" type="text/css">

    {{-- Datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    {{-- Datatables --}}


	  <title>Certificates</title>

    <style type="text/css">
      .hello{
        font-size: 30px;
      }
      .active::before{
        background: white;
      }
      .active{
        color: #D84315;
      }

    </style>

    <script>
      $(function(){
            $("#certsTable").DataTable();
        });
    </script>

</head>
<body>

<header class="header">
  <p class="hello">Hello, {{session('username')}}</p>
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="seminars/" class="topnav__link">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="templates/" class="topnav__link">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="generator/" class="topnav__link">Certificate Generator</a>
    </li>
    <li class="topnav__item ">
      <a href="certs/" class="topnav__link active">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="/logout" class="topnav__link">Logout</a>
    </li>
  </ul>
</header>



<div class="containe my-5">
  <div class="row mb-5">
    <h1 class="display-3 text-center">Generated Certificates</h1>
  </div>
  <div class="row">
    <div class="col-sm-8 mx-auto">
      <table class="table w-100 mx-auto" id="certsTable">
        <thead class="table-dark">
          <tr>
            <th>ID</th>
            <th>Image</th>

            <th>Awardee</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($certificates as $certificate)
          <tr>
            <td>{{$certificate->id}}</td>
            <td>
              <a target="_blank" href="{{$certificate->img_path}}"><img class="img-fluid" style="width: 300px;" src="{{$certificate->img_path}}" alt="..."></a>
            </td>
            <td>
              {{$certificate->awardee}}
            </td>
            
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>



</body>
</html>