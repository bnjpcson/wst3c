<!DOCTYPE html>
<html>
<head>
  	<link rel="icon" href="/assets/fav.png" type="image/x-icon">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
  	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link href="/css/A_home.css" rel="stylesheet" type="text/css">

        
	  <title>Admin Home</title>

    <style type="text/css">
      .hello{
        font-size: 30px;
      }
      .active::before{
        background: white;
      }
      .active{
        color: #D84315;
      }
    </style>

</head>
<body>

<header class="header">
  <p class="hello">Hello, {{session('username')}}</p>
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="{{url('seminars')}}" class="topnav__link active">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="{{url('templates')}}" class="topnav__link">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="{{url('generator')}}" class="topnav__link">Certificate Generator</a>
    </li>
    <li class="topnav__item">
      <a href="{{url('certs')}}" class="topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="{{url('logout')}}" class="topnav__link">Logout</a>
    </li>
  </ul>
</header>

<div class="container">
  @if(Session::has('success-seminar'))
  <div class="alert alert-success">
      {{ Session::get('success-seminar') }}
      @php
          Session::forget('success-seminar');
      @endphp
  </div>
  @endif
</div>



<div class="card mb-3 col-sm-6 mx-auto mt-4">
  {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}

  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif

  <div class="card-body">
    <div class = "container col-sm-12">
    @foreach ($seminars as $seminar)
		<form action="/seminars/edit/{{$seminar->id}}" method = "post" enctype="multipart/form-data">
	        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
	        <div class="form-row">
	            <div class="input-group mb-3">
	                <span class="input-group-text text-dark" style="background-color: #FFAB91;">Title</span>
	                <input type="text" name="title" class="form-control" value="{{$seminar->title}}">
	            </div>
	            <div class="input-group mb-3">
	                <span class="input-group-text text-dark" style="background-color: #FFAB91;">Date</span>
	                <input type="date" class="form-control" name="date" value="{{$seminar->date}}">
	            </div>
	            <div class="input-group mb-3">
	                <span class="input-group-text text-dark" style="background-color: #FFAB91;">Venue</span>
	                <input type="text" name="venue" class="form-control"  value="{{$seminar->venue}}">
	            </div>


	            <center>
	            	<button type="submit" class="btn btn-primary btn-block mx-auto">Update Seminar</button>
	            </center>
	        </div>
	    </form>
    @endforeach
	</div>
  </div>
</div>


</body>
</html>