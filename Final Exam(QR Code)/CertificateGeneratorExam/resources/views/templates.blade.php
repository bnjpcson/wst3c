<!DOCTYPE html>
<html>
<head>
  	<link rel="icon" href="/assets/fav.png" type="image/x-icon">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
  	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link href="/css/A_home.css" rel="stylesheet" type="text/css">

    {{-- Datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    {{-- Datatables --}}
    
	  <title>Templates</title>

    <style type="text/css">
      .hello{
        font-size: 30px;
      }
      .active::before{
        background: white;
      }
      .active{
        color: #D84315;
      }
    </style>

</head>
<body>

<header class="header">
  <p class="hello">Hello, {{session('username')}}</p>
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="seminars/" class="topnav__link">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="templates/" class="topnav__link active">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="generator/" class="topnav__link">Certificate Generator</a>
    </li>
    <li class="topnav__item">
      <a href="certs/" class="topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="/logout" class="topnav__link">Logout</a>
    </li>
  </ul>
</header>



<div class="card mb-3 col-sm-6 mx-auto mt-4">
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  @if(Session::has('success-template'))
    <div class="alert alert-success">
        {{ Session::get('success-template') }}
        @php
            Session::forget('success-template');
        @endphp
    </div>
  @endif


  

  <div class="card-body">
    <div class = "container col-sm-12">
		<form action = "" method = "post" enctype="multipart/form-data">
	        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
	        <div class="form-row">
     
              <div class="input-group">
               
                <span  class="input-group-text text-dark" style="background-color: #FFAB91;">Seminar</span>
                <select class="form-select" aria-label="#note" name="seminar">
                  @foreach ($seminars as $seminar)
                  <option value="{{$seminar->title}}">{{$seminar->title}}</option>
                  @endforeach
                </select>
                <span aria-describedby="emailHelp"></span>

               
                
              </div>  
              <div id="emailHelp" class="form-text">Only one template per seminar</div>
	            <div class="input-group my-3">
	                <span class="input-group-text text-dark" style="background-color: #FFAB91;">Contents</span>
	                <textarea type="text" name="contents" value ="<?php if (isset($_POST['preview'])) {echo $_POST['contents'];}?>" class="form-control"><?php if (isset($_POST['preview'])) {echo $_POST['contents'];}?></textarea>
	            </div>
	            <div class="input-group mb-3">
	                <span class="input-group-text text-dark" style="background-color: #FFAB91;">Logo</span>
	                <input type="file" name="logo" class="form-control" required>
	            </div>

              <div class="input-group mb-3">
                <span class="input-group-text text-dark" style="background-color: #FFAB91;">Signatory Name 1</span>
                <input type="text" name="signame1" class="form-control">
                <span class="input-group-text text-dark" style="background-color: #FFAB91;">E-signature 1</span>
                <input type="file" name="esig1" class="form-control">
                </div>
                <div class="input-group mb-3">
                <span class="input-group-text text-dark" style="background-color: #FFAB91;">Signatory Name 2</span>
                <input type="text" name="signame2" class="form-control">
                <span class="input-group-text text-dark" style="background-color: #FFAB91;">E-signature 2</span>
                <input type="file" name="esig2" class="form-control">
              </div>
              
	            <center>
	            	<button type="submit" name="preview" class="btn btn-primary btn-lg btn-block mx-auto">Preview</button>
	            </center>
	        </div>
	    </form>
	</div>
  </div>
</div>


<?php 
      if (isset($_POST['preview'])) {
        $seminar = $_POST['seminar'];
        // $title = $_POST['title'];
        // $title_len = strlen($_POST['title']);
        $contents= $_POST['contents'];
        // $url = $_POST['url'];
        $signame1 = $_POST['signame1'];
        $signame2 = $_POST['signame2'];
        $image_esig1 = $_FILES['esig1']['name'];

        $image_type = $_FILES['esig1']['type'];
        $image_size = $_FILES['esig1']['size'];
        $image_tmp_name= $_FILES['esig1']['tmp_name'];
        move_uploaded_file($image_tmp_name,"signatures/"."$image_esig1");



        $image_esig2 = $_FILES['esig2']['name'];
        $image_type = $_FILES['esig2']['type'];
        $image_size = $_FILES['esig2']['size'];
        $image_tmp_name= $_FILES['esig2']['tmp_name'];
        move_uploaded_file($image_tmp_name,"signatures/"."$image_esig2");


        $image_name = $_FILES['logo']['name'];
        $image_type = $_FILES['logo']['type'];
        $image_size = $_FILES['logo']['size'];
        $image_tmp_name= $_FILES['logo']['tmp_name'];
        move_uploaded_file($image_tmp_name,"logos/"."$image_name");

        if ($contents) {
          $font_size_content = 10;
        }

        if ($contents == "") {
          echo 
          "
          <div class='mx-auto alert alert-danger col-sm-6 text-center' role='alert'>
              Ensure you fill all the fields!
          </div>
          ";
        }else{
          echo 
          "
          <div class='mx-auto alert alert-success col-sm-6 text-center' role='alert' text-center>
              Here's the preview.
          </div>
          ";

          //designed certificate picture
          $image = "assets/template.png";

          $createimage = imagecreatefrompng($image);

          //this is going to be created once the generate button is clicked
          $output = "previews/"."newpreview.png";

          //then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
          $white = imagecolorallocate($createimage, 205, 245, 255);
          $black = imagecolorallocate($createimage, 0, 0, 0);

          //Then we make use of the angle since we will also make use of it when calling the imagettftext function below
          $rotation = 0;

          //we then set the x and y axis to fix the position of our text name
          $origin_x = 200;
          $origin_y=250;


          //we then set the x and y axis to fix the position of our text occupation
          $origin1_x = 185;
          $origin1_y=300;

          //we then set the differnet size range based on the length of the text which we have declared when we called values from the form
          // if($title_len<=7){
          //   $font_size = 25;
          //   $origin_x = 190;
          // }
          // elseif($title_len<=12){
          //   $font_size = 30;
          // }
          // elseif($title_len<=15){
          //   $font_size = 26;
          // }
          // elseif($title_len<=20){
          //    $font_size = 18;
          // }
          // elseif($title_len<=22){
          //   $font_size = 15;
          // }
          // elseif($title_len<=33){
          //   $font_size=11;
          // }
          // else {
          //   $font_size =10;
          // }

          //$certificate_text = $title;

          //font directory for name
          $drFont = "assets/Allura-Regular.TTF";

          // font directory for occupation name
          $drFont1 = "assets/Poppins-SemiBold.TTF";

          //function to display name on certificate picture
          //$text1 = imagettftext($createimage, $font_size, $rotation, $origin_x, $origin_y, $black,$drFont, $certificate_text);

          //function to display occupation name on certificate picture
          $text2 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, $origin1_y, $black, $drFont1, $contents);

          $text3 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, 30, $black, $drFont1, $seminar);

          $text4 = imagettftext($createimage, $font_size_content, $rotation, 250, 390, $black, $drFont1, $signame1);

          $text5 = imagettftext($createimage, $font_size_content, $rotation, 470, 390, $black, $drFont1, $signame2);



          $esig1=imagecreatefrompng("signatures/"."$image_esig1");
          imagecopy($createimage,$esig1, 250, 330, 0, 0, 100, 50);



          $esig2=imagecreatefrompng("signatures/"."$image_esig2");
          imagecopy($createimage,$esig2, 460, 330 , 0, 0, 100, 50);

          // $text3 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, $origin1_y, $black, $drFont1, $seminar);

          $str=imagecreatefrompng("logos/"."$image_name");
          imagecopy($createimage,$str, 20, 30 , 0, 0, 100, 100);

          // $QR=imagecreatefromstring(file_get_contents("https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=".$url."&choe=UTF-8"));
		      // imagecopyresampled($createimage, $QR, 15, 352 , 0, 0, 110, 110, 100,100);

          imagepng($createimage,$output,3);

 ?>


        <!-- this displays the image below -->
        <div class = "container col-sm-6">
        	<img class="mx-auto d-block" src="<?php echo $output; ?>" >
        </div>
        
        <center>
          <div class="mt-4">
            <a href="templates/save/<?php echo $seminar; ?>" class="btn btn-success">Add Design</a>
          </div>
        </center>

        <!-- this provides a download button -->
        
        <br><br>
<?php 
        }
      }

     ?>

<script>
  $(function(){
        $("#templatesTable").DataTable();
    });
</script>

<div class="container">
  <div class="row my-5">
    <div class="col-sm-8 mx-auto">
      <table class="table w-100 mx-auto" id="templatesTable">
        <thead class="bg-dark text-white">
          <tr>
            <th>ID</th>
            <th>Seminar Title</th>
            <th>Template</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($templates as $template)
          <tr>
            <td>{{$template->id}}</td>
            <td>
              {{$template->seminar_name}}
              
            </td>
            <td>
              <a target="_blank" href="{{$template->img_path}}"><img class="img-fluid w-50" src="{{$template->img_path}}" alt="{{$template->seminar_name}}"></a>
              
            </td>
            <td>
              <div class="text-center">
                <a href="/templates/delete/{{$template->id}}" class="ms-2" > <i class="fa fa-trash text-danger" aria-hidden="true"></i></a>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>



</body>
</html>