**FINAL EXAM QR CODE GENERATOR**

---

## Instructions

Admin

User access type
Admin - can manage all features
Editor
User

CRUD for seminar/training management with specific certificate template with preview
note: 
content, signatures, logo are dynamic
Use data tables for all listing
we should be able to flag each seminar/training settings active and not active

Client
The unique QR code is printed on certificates
When the user scans he will be directed to a page to check if the certificate is original
Or manual searching of verifying certificate authenticity through code
The user scans a unique URL that contains a token that allows us to check if the certificate is real.
