<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ERROR 404</title>

    <!-- Bootstrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap 5 -->

    <!-- In-Line CSS -->
    <style>
        .form-label{
            font-size: 20px;
            font-weight: bold;
        }
        .row{
            margin-top: 20px;
        }
    </style>

</head>
<body>
    <div class="container my-5">
        <div class="row">
            <div class="col-sm-12 bg-danger text-white">
                <h1 class="text-center">ERROR 404</h1>
            </div>
        </div>
        
    </div>
</body>
</html>