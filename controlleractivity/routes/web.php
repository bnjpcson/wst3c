<?php

use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});


//Customer ID, Name, Address/age(optional)  
//using the type in the URL customer/{variable1}/{variable2}/{variableN}
Route::get('/customer/{cusID}/{cusName}/{cusAddress}', [OrderController::class, 'displayCustomer']);

//it requires Item No, Name, and Price as required parameters;
Route::get('/item/{itemNo}/{itemName}/{price}',[OrderController::class, 'displayItem']);

//it requires Customer ID, Name, Order No, and Date as required parameters;
Route::get('/order/{cusID}/{name}/{orderNum}/{date}',[OrderController::class, 'displayOrder']);

//it requires TransNo, Order No, Item ID, Name, Price, Qty,  as required parameters 

Route::get('/orderDetails/{TransNo}/{orderNum}/{itemID}/{name}/{price}/{qty}',[OrderController::class, 'displayOrderDetails']);
