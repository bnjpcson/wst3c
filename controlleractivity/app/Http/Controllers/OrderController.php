<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    public function displayCustomer($cusID, $cusName, $cusAddress)
    {
        //
        return view('customer', ['cusID' => $cusID, 'cusName' => $cusName, 'cusAddress' => $cusAddress]);
    }

    public function displayItem($itemNo, $itemName, $price)
    {
        //
        return view('item', ['itemNo' => $itemNo, 'itemName' => $itemName, 'price' => $price]);
    }

    public function displayOrder($cusID, $name, $orderNum, $date)
    {
        //
        return view('order', ['cusID' => $cusID, 'name' => $name, 'orderNum' => $orderNum, 'date' => $date]);
    }

    public function displayOrderDetails($TransNo, $orderNum, $itemID, $name, $price, $qty)
    {
        //
        return view('orderDetails', ['TransNo' => $TransNo, 'orderNum' => $orderNum, 'itemID' => $itemID, 'name' => $name, 'price' => $price, 'qty' => $qty]);
    }


}
