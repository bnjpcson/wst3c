**Midterm Exam (Laboratory**

---

## Instructions

Based on the lessons that we’ve learned, the micro system must pass parameters while 
navigating the customer/, item/, order/, and orderDetails/ and display all the output accordingly. 
You must create routes to access the URL and its required parameters.
You must create a route using the format /<<name of the route>>/{variable1}/{variable2}/{variableN}.

For example, if the user will navigate to the customer/,  
it requires 3 parameters namely the Customer ID, Name, Address/age(optional)  using the type in the URL customer/{variable1}/{variable2}/{variableN}. 
If the user will navigate to the item/, it requires Item No, Name, and Price as required parameters;
If the user will navigate to the order/, it requires Customer ID, Name, Order No, and Date as required parameters;
If the user will navigate to the orderDetails/, it requires TransNo, Order No, Item ID, Name, Price, Qty,  as required parameters and receipt number as optional. Then will compute the total price.
turn in screenshot each scenario then push your output to your repo.