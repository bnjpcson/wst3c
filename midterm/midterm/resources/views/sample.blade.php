<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sample</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Bootstrap 5 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
        <!-- Bootstrap 5 -->

        <!-- In-Line CSS -->
        <style>
            .form-label{
                font-size: 20px;
                font-weight: bold;
            }
            .row{
                margin-top: 20px;
            }
        </style>

        
    </head>
    
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Name: Benjie Pecson</h3>
                </div>
                <div class="col-sm-6 text-end">
                    <h3>Section: IT-3C</h3>
                </div>
            </div>
        </div>
    </body>
</html>
