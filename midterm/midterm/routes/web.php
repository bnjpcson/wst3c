<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('sample');
});

Route::get('/welcome', function () {
    return view('welcome');
});

//Customer ID, Name, Address

//it requires 3 parameters namely the 
//Customer ID, Name, Address/age(optional)  
//using the type in the URL customer/{variable1}/{variable2}/{variableN}
Route::get('/customer/{cusID}/{cusName}/{cusAddress?}',function($cusID, $cusName, $cusAddress=null){

    return view('customer', ['cusID' => $cusID, 'cusName' => $cusName, 'cusAddress' => $cusAddress]);
    
});

//If the user will navigate to the item/, 
//it requires Item No, Name, and Price as required parameters;
Route::get('/item/{itemNo}/{itemName}/{price}',function($itemNo, $itemName, $price){

    return view('item', ['itemNo' => $itemNo, 'itemName' => $itemName, 'price' => $price]);
    
});

//If the user will navigate to the order/, 
//it requires Customer ID, Name, Order No, and Date as required parameters;
Route::get('/order/{cusID}/{name}/{orderNum}/{date}',function($cusID, $name, $orderNum, $date){

    return view('order', ['cusID' => $cusID, 'name' => $name, 'orderNum' => $orderNum, 'date' => $date]);
    
});

//If the user will navigate to the orderDetails/, 
//it requires TransNo, Order No, Item ID, Name, Price, Qty,  as required parameters 
//and receipt number as optional. Then will compute the total price.

Route::get('/orderDetails/{TransNo}/{orderNum}/{itemID}/{name}/{price}/{qty}/{receiptNo?}',function($TransNo, $orderNum, $itemID, $name, $price, $qty, $receiptNo){

    $total = $price*$qty;
    return view('orderDetails', ['TransNo' => $TransNo, 'orderNum' => $orderNum, 'itemID' => $itemID, 'name' => $name, 'price' => $price, 'qty' => $qty, 'receiptNo' => $receiptNo, 'total' => $total]);
    
});

