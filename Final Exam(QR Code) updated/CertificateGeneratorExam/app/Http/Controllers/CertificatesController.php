<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Seminar;
use Illuminate\Http\Request;

class CertificatesController extends Controller
{
    //
    public function displayCertificates(Request $request){
        $certificates = Certificate::all();
        $seminars = Seminar::all();

        if ($request->session()->has('username')) {
            return view('certificates', ['certificates' => $certificates, 'seminars' => $seminars]);
        }else{
            return redirect('admin');
        }
    }
}
