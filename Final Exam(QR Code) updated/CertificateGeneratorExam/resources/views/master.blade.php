<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="/assets/fav.png" type="image/x-icon">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
  	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link href="/css/A_home.css" rel="stylesheet" type="text/css">

    {{-- Datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    {{-- Datatables --}}

    <style type="text/css">
      .hello{
        font-size: 30px;
      }
      .active::before{
        background: white;
      }
      .active{
        color: #FFBF00;
      }
    </style>
    @stack('css-external')
    @stack('css-internal')


    {{-- Title --}}
    @yield('title')

    {{-- Links --}}
    @yield('links')

</head>
<body>

 

  <script>function display_ct6() {
    var x = new Date()
    var ampm = x.getHours( ) >= 12 ? ' PM' : ' AM';
    hours = x.getHours( ) % 12;
    hours = hours ? hours : 12;
    var x1=x.getMonth() + 1+ "/" + x.getDate() + "/" + x.getFullYear(); 
    x1 = x1 + " - " +  hours + ":" +  x.getMinutes() + ":" +  x.getSeconds() + ":" + ampm;
    document.getElementById('ct6').innerHTML = x1;
    display_c6();
     }
     function display_c6(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_ct6()',refresh)
    }
    display_c6()
    </script>

    
@yield('content')
   



    <div class="container-fluid mt-5" style="background-color: #082b54">
        <div class="row pt-5">
            <div class="col-sm-6 mx-auto">
             
                <div class="row py-3 d-flex justify-content-center text-center text-white">
                    <span class="text-white">
                        &copy; {{ date('Y') }} {{ config('app.name')}}. @lang('All rights reserved.') 
                    </span>        
                    {{-- Design by <a class="link-primary" style="text-decoration: none;" href="https://www.linkedin.com/in/bnjpcson/">Bnjpcson</a> --}}
     
                </div>
                <div class="row py-3 d-flex justify-content-center text-center ">
                    <div class="col-sm-12">
                        <a href="https://www.facebook.com/bnjpcson" class="p-3"><img src="assets/developers/benjie.jpg" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/No.0ne.kn0ws.mee" class="p-3"><img src="assets/developers/jonel.png" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://facebook.com/katorinagami" class="p-3"><img src="assets/developers/kat.png" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/Windeeeeeeeeeeeel" class="p-3"><img src="assets/developers/windel.png" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/Louie.Catabay18" class="p-3"><img src="assets/developers/louie.png" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        
                        
                        
                    </div>
                    <div class="col-sm-12 py-3">
                        
                        <a href="https://www.facebook.com/itsmeeeecmllll" class="p-3"><img src="assets/developers/camila.png" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/rhaemonette" class="p-3"><img src="assets/developers/july.png" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/jroger24" class="p-3"><img src="assets/developers/jroger.png" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/freakaphrodite23" class="p-3"><img src="assets/developers/demii.png" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        
                    </div>
                </div>
                

            </div>
        </div>
        
    </div>

    <script>
      function date_time(id) {
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if (h < 10) {
        h = "0" + h;
        }
        m = date.getMinutes();
        if (m < 10) {
        m = "0" + m;
        }
        s = date.getSeconds();
        if (s < 10) {
        s = "0" + s;
        }
        result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("' + id + '");', '1000');
        return true;
      }
    </script>
</body>
</html>