@extends('master')

@section('title')
    <title>Home</title>
@endsection

@section('content')

    <nav class="navbar sticky-top navbar-expand-lg navbar-light py-1 bg-light">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">
                {{-- <img class="" style="width:50px;" src="{{ asset("assets/images/printplusplus.png") }}" alt="Logo" id="logo"> --}}
                PRINTPLUSPLUS
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link active" aria-current="page" href="{{ url('/') }}">Home</a>
                    <a class="nav-link" href="{{ url('/pricing') }}">Pricing</a>
                    <a class="nav-link" href="{{ url('/about') }}">About</a>
                    <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                    <a class="nav-link" href="{{ url('/login') }}">Login</a>
                </div>
            </div>
        </div>
    </nav>


   
    {{-- <div class="container-fluid bg-unwave">
    </div> --}}

    <div class="container-fluid bg">
        <div class="row ">
            <div class="col-sm-12">

                <div class="container text-white" style="margin-top: 170px;">
                    <div class="row my-auto">
                        <div class="col-sm-6">
                            <h1 class="display-1">PrintPlusPlus</h1>
                            <p>Fast, accurate printing, good layout design, and competitive prices. We also provide photocopying and layout services with low cost but high quality.</p>
                            <a href="contact" class="btn btn-outline-light mt-3">Contact us now</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h1 class=" text-center">Services</h1>
                <hr>
            </div>
        </div>
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            <div class="col mb-5">
                <div class="card h-100">
                    <img class="card-img-top h-50" src="{{ asset('assets/images/photocopy.webp') }}" alt="..." />
                    <div class="card-body p-4">
                        <div class="">
                            <!-- Product name-->
                            <h5 class="fw-bolder text-center">Print</h5>
                            <!-- Product price-->
                            <ul>
                                <li>Any page/paper size</li>
                                <li>Tarpaulin</li>
                                <li>Photo</li>
                                <li>T-Shirt</li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="col mb-5">
                <div class="card h-100">
                    <img class="card-img-top h-50" src="{{ asset('assets/images/layout.webp') }}" alt="..." />

                    <div class="card-body p-4">
                        <div class="">
                            <h5 class="fw-bolder text-center">Layout Design</h5>
                            <!-- Product price-->
                            <ul>
                                <li>Logo</li>
                                <li>Business Cards</li>
                                <li>Invitation Cards</li>
                                <li>Tarpaulin</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col mb-5">
                <div class="card h-100">
                    <img class="card-img-top h-50" src="{{ asset('assets/images/print.webp') }}" alt="..." />

                    
                    <!-- Product details-->
                    <div class="card-body p-4">
                        <div class="">
                            <h5 class="fw-bolder text-center">Photocopy</h5>
                            <!-- Product price-->
                            <ul>
                                <li>Letter/Legal Size Paper</li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h1 class=" text-center">The Management Team</h1>
                <hr>
            </div>
        </div>

        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            <div class="col mb-5">
                <div class="container bg-secondary">
                    <div class="mx-auto p-5">
                        <i class="fa fa-user fa-5x text-white" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <h5 class="fw-bold txt">Marc Adrian Bautista</h5>
                    <h6 class=" text"><em>Financial Analyst</em></h6>
                </div>
            </div>

            <div class="col mb-5">
                <div class="container bg-secondary">
                    <div class="mx-auto p-5">
                        <i class="fa fa-user fa-5x text-white" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <h5 class="fw-bold">Benjie Pecson</h5>
                    <h6 class=" text"><em>Team Leader</em></h6>
                </div>
            </div>
            <div class="col mb-5">
                <div class="container bg-secondary">
                    <div class="mx-auto p-5">
                        <i class="fa fa-user fa-5x text-white" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <h5 class="fw-bold">John Doe</h5>
                    <h6 class=" text"><em>Staff I</em></h6>
                </div>
            </div>
            <div class="col mb-5">
                <div class="container bg-secondary">
                    <div class="mx-auto p-5">
                        <i class="fa fa-user fa-5x text-white" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <h5 class="fw-bold">Juan Dela Cruz</h5>
                    <h6 class="text"><em>Staff II</em></h6>
                </div>
            </div>
        </div>
    </div>
    
@endsection

