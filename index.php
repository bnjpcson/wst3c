<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Create repository in bitbucket</title>
</head>

<style>
  table{
    margin: auto;
    border-collapse: collapse;
  }
  td{
    border: 1px solid black;
    padding: 5px;
  }
</style>
<body>
  <table>
    <tr>
      <td>Name</td>
      <td>Benjie Pecson</td>
    </tr>
    <tr>
      <td>Year and Section</td>
      <td>3-C</td>
    </tr>
    <tr>
      <td>Subject</td>
      <td>Elective 1 (Web Systems and Technologies 2)</td>
    </tr>
    <tr>
      <td>Date and Time</td>
      <td>March 1, 2022 - 12:10 PM</td>
    </tr>
  </table>
</body>
</html>