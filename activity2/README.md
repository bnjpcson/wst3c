**Activity laravel #1**

---

## Instructions
Create your own portfolio that consist of 5 pages. All information and images should be all about you
- homepage
- about me / include your digital resume
- registration
- login form
- contact us/ gallery

Attached are the example pages for your reference apply all learnings we had during lectures from form validation to styling of the pages.
https://www.elegantthemes.com/layouts/lifestyle/travel-blog-landing-page/live-demo
https://www.elegantthemes.com/layouts/art-design/copywriter-landing-page/live-demo
https://www.elegantthemes.com/layouts/art-design/graphic-illustrator-landing-page/live-demo
https://www.elegantthemes.com/layouts/fashion-beauty/personal-stylist-landing-page/live-demo

Attached all screenshot of each pages 
push all your code and output to your repo