<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>My Portfolio</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Boostrap 5 -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

        <!-- font awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                
            }

            .bg{
                background-image: url("images/bg.jpg");

                background-position: center;
                background-size: cover;
                min-height: 100%;
                opacity: 90%;

                height: 650px;
            }

            .navbar{
                font-weight: bold;
            }

            .navbar-brand{
                padding: 5px 20px;
                background-color: white;
                letter-spacing: 3px;
                font-size: 25px;
            }
            #my-image{
                width: 50%;
                margin-left: 25%;
            }
            .sample-text{
                
                margin-top: 150px;
            }
            .sample-text h1{
                font-weight: bold;
                font-size: 100px;
            }
            #btnAbout{
                padding: 10px 20px;
                background-color: #f7670a;
                color: white;
                text-decoration: none;
                border-radius: 50px;
                font-weight: bold;
                font-size: 20px;
            }
            #btnAbout:hover{
                background-size: 4px 50px;
                background-color: #b56b59;
                color: white;
            }
           

        </style>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-light py-3 bg-white">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">Bnjpcson</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link active" aria-current="page" href="{{ url('/') }}">Home</a>
                    <a class="nav-link" href="{{ url('/about') }}">About</a>
                    <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                    <a class="nav-link" href="{{ url('/registration') }}">Registration</a>
                    <a class="nav-link" href="{{ url('/login') }}">Login</a>
                </div>
            </div>
        </div>
    </nav>

    <div class="container-fluid bg text-white">
        <div class="row">
            <div class="col-sm-12 text-center sample-text">
                <h1>Hello, I'm Benj</h1>
                
            </div>
            <div class="col-sm-12 text-center text-light">
                <h2>An IT student in PSU-Urdaneta City</h2>
            </div>

            <div class="col-sm-12 text-center text-light my-5" >
                <a href="{{ url('/about') }}" id="btnAbout">MORE ABOUT ME</a>
            </div>
        </div>
    </div>

    <div class="container-fluid my-5">
        <div class="row">
            <div class="col-sm-6 my-5">
                
                <img src="{{ asset('images/image1.jpg') }}" alt="My Image" id="my-image">
                
            </div>
            <div class="col-sm-6">
                <div class="container">
                    <div class="row">
                        <div class="col">
                        <h3>LET'S START A CONVERSATION</h3><hr>
                        </div>
                    </div>
                    <form action="">
                    <div class="row my-3">
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Name" required>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Email Address" required>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Subject" required>
                        </div>
                    </div>

                    <div class="row my-3">
                        <div class="col-sm-8">
                            <textarea id="textarea" cols="30" rows="10" class="form-control" placeholder="Message" required></textarea>
                        </div>
                    </div>
                    <div class="row my-3 text-end">
                        <div class="col-sm-8">
                            <button class="btn btn-success">SEND MESSAGE</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <footer class="container-fluid text-center bg-dark text-white p-5">
        <div class="row my-3">
            <div class="col">
            <a href="#" class="btn btn-light border"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
            </div>
        </div>
        
        <div class="row my-4">
            <div class="col">
                <style>
                    .icon{
                        text-decoration: none;
                        color: white;
                        font-size: 30px;
                    }
                </style>
                <a href="https://www.facebook.com/bnjpcson/"><i class="fa fa-facebook-official icon px-1"></i></a>
                <a href="https://www.instagram.com/bnjpcson/"><i class="fa fa-instagram icon px-1"></i></a>
                <a href="https://twitter.com/bnjpcson"><i class="fa fa-twitter icon px-1"></i></a>
                <a href="https://www.linkedin.com/in/bnjpcson/"><i class="fa fa-linkedin icon px-1"></i></a>
                
            </div>
        </div>
        
        <div class="row my-3">
            <div class="col">
                <p>&copy Copyright 2022</p>
            </div>
        </div>
        
    </footer>

    </body>
</html>
