<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>My Portfolio | About</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Boostrap 5 -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

        <!-- font awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                
            }


            .navbar{
                font-weight: bold;
            }

            .navbar-brand{
                padding: 5px 20px;
                background-color: white;
                letter-spacing: 3px;
                font-size: 25px;
            }

            .sample-text{
                
                margin-top: 150px;
            }
            .sample-text h1{
                font-weight: bold;
                font-size: 100px;
            }
           

        </style>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-light py-3 bg-white">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">Bnjpcson</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                    <a class="nav-link active" aria-current="page"  href="{{ url('/about') }}">About</a>
                    <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                    <a class="nav-link" href="{{ url('/registration') }}">Registration</a>
                    <a class="nav-link" href="{{ url('/login') }}">Login</a>
                </div>
            </div>
        </div>
    </nav>

    <style>
        #my-image{
            background-image: url("images/image7.jpg");
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            width: 250px;
            border-radius: 50%;
            object-fit: fill;
        }
    </style>

    <div class="container-fluid" style="opacity: 90%; background-color: #DBD7D7;">
        <div class="row">
            <div class="col-sm-4">
                <div class="container-fluid bg-dark text-white" style="height:950px;">
                    <div class="row">
                        <div class="col-sm-6 mx-auto my-5" id="my-image" style="height: 250px;">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mx-auto text-center">
                            <h1 class="fw-bold my-3">PECSON, BENJIE AGUILOS</h1>
                            <h4 class="my-3">#07 Rizal St. Poblacion, District III. Pozorrubio, Pangasinan</h4>
                            <h4 class="my-3">09454913789</h4>
                            <h4 class="my-3 text-decoration-underline">pecsonbenjiea@gmail.com</h4>

                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mx-auto text-center mt-5">
                            <style>
                                .icon{
                                    text-decoration: none;
                                    color: white;
                                    font-size: 30px;
                                }
                            </style>
                            <a href="https://www.facebook.com/bnjpcson/"><i class="fa fa-facebook-official icon px-1"></i></a>
                            <a href="https://www.instagram.com/bnjpcson/"><i class="fa fa-instagram icon px-1"></i></a>
                            <a href="https://twitter.com/bnjpcson"><i class="fa fa-twitter icon px-1"></i></a>
                            <a href="https://www.linkedin.com/in/bnjpcson/"><i class="fa fa-linkedin icon px-1"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="container-fluid my-5">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="fw-bold">Personal Information</h1><hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-2">Age</div>
                                <div class="col-1">:</div>
                                <div class="col-9">20 Years Old</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Birth Date</div>
                                <div class="col-1">:</div>
                                <div class="col-9">April 30, 2001</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Birth Place</div>
                                <div class="col-1">:</div>
                                <div class="col-9">Pozorrubio, Pangasinan</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Mother</div>
                                <div class="col-1">:</div>
                                <div class="col-9">Jocelyn A. Pecson</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Father</div>
                                <div class="col-1">:</div>
                                <div class="col-9">Benjamin C. Pecson Jr. (Deceased)</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Gender</div>
                                <div class="col-1">:</div>
                                <div class="col-9">Male</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Civil Status</div>
                                <div class="col-1">:</div>
                                <div class="col-9">Single</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Citizenship</div>
                                <div class="col-1">:</div>
                                <div class="col-9">Filipino</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Religion</div>
                                <div class="col-1">:</div>
                                <div class="col-9">Roman Catholic</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Height</div>
                                <div class="col-1">:</div>
                                <div class="col-9">170cm</div>
                            </div>
                            <div class="row">
                                <div class="col-2">Weight</div>
                                <div class="col-1">:</div>
                                <div class="col-9">70kg</div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <h1 class="fw-bold">Educational Background</h1><hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-2">Tertiary</div>
                                <div class="col-1">:</div>
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col fw-bold">Pangasinan State University - Urdaneta City</div>
                                    </div>
                                    <div class="row">
                                        <div class="col">A.Y. 2019 - Present</div>
                                    </div>
                                    <div class="row">
                                        <div class="col">Brgy. San Vicente, Urdaneta City, Pangasinan</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-2">Secondary </div>
                                <div class="col-1">:</div>
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col fw-bold">Benigno V. Aldana National High School</div>
                                    </div>
                                    <div class="row">
                                        <div class="col fw-bold">Senior High School</div>
                                    </div>
                                    <div class="row">
                                        <div class="col">S.Y. 2017-2019</div>
                                    </div>
                                    <div class="row">
                                        <div class="col">Brgy.Cablong Pozorrubio, Pangasinan</div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col fw-bold">Benigno V. Aldana National High School</div>
                                    </div>
                                    <div class="row">
                                        <div class="col fw-bold">Junior High School</div>
                                    </div>
                                    <div class="row">
                                        <div class="col">S.Y. 2013-2017</div>
                                    </div>
                                    <div class="row">
                                        <div class="col">Brgy.Cablong Pozorrubio, Pangasinan</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-2">Primary</div>
                                <div class="col-1">:</div>
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col fw-bold">Pozorrubio Central School</div>
                                    </div>
                                    <div class="row">
                                        <div class="col">S.Y. 2006 - 2013</div>
                                    </div>
                                    <div class="row">
                                        <div class="col">Caballero St., Pozorrubio, Pangsinan</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- <div class="container-fluid text-center bg-dark text-white p-5">
        <div class="row my-3">
            <div class="col">
            <a href="#" class="btn btn-light border"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
            </div>
        </div>
        
        <div class="row my-4">
            <div class="col">
                <style>
                    .icon{
                        text-decoration: none;
                        color: white;
                        font-size: 30px;
                    }
                </style>
                <a href="https://www.facebook.com/bnjpcson/"><i class="fa fa-facebook-official icon px-1"></i></a>
                <a href="https://www.instagram.com/bnjpcson/"><i class="fa fa-instagram icon px-1"></i></a>
                <a href="https://twitter.com/bnjpcson"><i class="fa fa-twitter icon px-1"></i></a>
                <a href="https://www.linkedin.com/in/bnjpcson/"><i class="fa fa-linkedin icon px-1"></i></a>
                
            </div>
        </div>
        
        <div class="row my-3">
            <div class="col">
                <p>&copy Copyright 2022</p>
            </div>
        </div>
        
    </div> -->

    </body>
</html>
