<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>My Portfolio | Registration</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Boostrap 5 -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

        <!-- font awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                
            }


            .navbar{
                font-weight: bold;
            }

            .navbar-brand{
                padding: 5px 20px;
                background-color: white;
                letter-spacing: 3px;
                font-size: 25px;
            }

            .sample-text{
                
                margin-top: 150px;
            }
            .sample-text h1{
                font-weight: bold;
                font-size: 100px;
            }
           

        </style>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-light py-3 bg-white">
        <div class="container-fluid">
            <a class="navbar-brand ms-4" href="{{ url('/') }}">Bnjpcson</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-auto me-5">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                    <a class="nav-link" href="{{ url('/about') }}">About</a>
                    <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                    <a class="nav-link active" aria-current="page" href="{{ url('/registration') }}">Registration</a>
                    <a class="nav-link" href="{{ url('/login') }}">Login</a>
                </div>
            </div>
        </div>
    </nav>

    <style>
        .login{
            border-radius: 5px;
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        }
        .logintxt{
            font-weight: bold;
            font-size: 50px;
        }
        .btn-success{
            opacity: 90%;
        }
    </style>

    <div class="container-fluid" style="height:650px; opacity: 90%; background-color: #DBD7D7;">
        <div class="row">
            <div class="col-sm-5 mx-auto">
                <div class="container bg-light my-5 login">
                    <div class="row">
                        <div class="col-sm-12 mt-5 text-center">
                            <h1 class="logintxt">Registration</h1><hr>
                        </div>
                    </div>

                    <form action="">
                        <div class="row">
                            <div class="col-sm-8 mx-auto my-2">
                                <label for="">Username<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8 mx-auto my-2">
                                <label for="">Password<span class="text-danger">*</span></label>
                                <input type="password" class="form-control" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8 mx-auto my-2">
                                <label for="">Confirm Password<span class="text-danger">*</span></label>
                                <input type="password" class="form-control" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8 mx-auto my-2">
                                <label for="">Email Address<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8 mx-auto">
                                <button class="btn btn-success w-100 mb-5 mt-3">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <footer class="container-fluid text-center bg-dark text-white p-5">
        <div class="row my-3">
            <div class="col">
            <a href="#" class="btn btn-light border"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
            </div>
        </div>
        
        <div class="row my-4">
            <div class="col">
                <style>
                    .icon{
                        text-decoration: none;
                        color: white;
                        font-size: 30px;
                    }
                </style>
                <a href="https://www.facebook.com/bnjpcson/"><i class="fa fa-facebook-official icon px-1"></i></a>
                <a href="https://www.instagram.com/bnjpcson/"><i class="fa fa-instagram icon px-1"></i></a>
                <a href="https://twitter.com/bnjpcson"><i class="fa fa-twitter icon px-1"></i></a>
                <a href="https://www.linkedin.com/in/bnjpcson/"><i class="fa fa-linkedin icon px-1"></i></a>
                
            </div>
        </div>
        
        <div class="row my-3">
            <div class="col">
                <p>&copy Copyright 2022</p>
            </div>
        </div>
        
    </footer>

    </body>
</html>
