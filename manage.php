<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>DBAIHS</title>
  
	  <link rel="stylesheet" type="text/css" href="home.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    
    </head>
        <body>
          <center>
              <div id="banner">
                <img class="img-fluid px-3 mt-3" style="width: 10%;" src="photos/DBAIHSlogo.png" alt="Banner">
                <img class="img-fluid px-3 mt-3" style="width: 10%;" src="photos/deped.png" alt="Banner">
                <img class="img-fluid px-3 mt-3" style="width: 10%;" src="photos/divlogo.png" alt="Banner">
                </div>
          </center>

          <div class="text">
            <p class="text-center m-4 font-weight-bold" style="font-size: 25px;">Don Benito Agro-Industrial High School<p>
        </div>

        <div class="">
            <p class="ml-5 text-start">Welcome, <strong><?php echo $_SESSION['username']; ?></strong></p>
        </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-success">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
      

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

          <li class="nav-item">
            <a class="nav-link text-white" href="home.php">USER ACCOUNTS</a>
          </li>

          <li class="nav-item">
            <a class="nav-link text-white" href="eval.php">EVALUATION/VERIFICATION</a>
          </li>

          <li class="nav-item">
            <a class="navbar-brand" href="#">MANAGE</a>
          </li>

          <li class="nav-item">
            <a class="nav-link text-white" href="#">GRADES</a>
          </li>
          
        </ul>

        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="" aria-label="Search">
          <button class="btn btn-dark my-2 my-sm-0" type="submit"><i class="far fa-search"></i></button>
        </form>

        &nbsp;
        &nbsp;
        <a href="home.php?logout='1'" class="btn btn-danger" role="button"><i class="fal fa-sign-out-alt"></i>&nbsp;&nbsp;Log Out</a>
      </div>
    </nav>



    <div class="container my-5 mx-auto">
      <div class="row">
        <div class="col">
          <select class="form-select" aria-label="Default select example">
            <option selected>Grade 7</option>
            <option selected>Grade 8</option>
            <option selected>Grade 9</option>
            <option selected>Grade 10</option>
          </select>
        </div>
        <div class="col">
          <select class="form-select" aria-label="Default select example">
            <option selected>Section 1</option>
            <option selected>Section 2</option>
          </select>
        </div>
      </div>
    </div>



    <br><br><br><br>


<footer class="bg-light text-white">
  <!-- Grid container -->  
  <div style="background-color: rgba(0, 0, 0, 0.2);">
    <div class=" p-3">
        <div class="row">
          <div class=" text-center col-md-6 mt-4">
            © 2021 Copyright:
            <a class="text-white" href="home.php/">DBAIHS</a>
          </div>
          <div class="col-md-6 text-center">
            <div class="container">
              <section class="">
                  <div class="caption">
                    <p class="text-dark">CONTACT US HERE</p>
                  </div>
                <a
                  class="btn btn-primary btn-floating"
                  style="background-color: #3b5998;"
                  href="#!"
                  role="button"
                  ><i class="fab fa-facebook-f"></i></a>
              </section>
            </div>
          </div>       
        </div>
      </div>
      
    </div>
  <!-- Copyright -->
</footer>

</body>
</html>