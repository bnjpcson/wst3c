<?php

use App\Http\Controllers\crudcontroller;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/welcome', function () {
//     return view('welcome');
// });


Route::get('/login',[LoginController::class, 'displayLogin']);
Route::post('/login',[LoginController::class, 'validateLogin']);


Route::get('/signup',[RegisterController::class, 'signupShow']);
Route::post('/signup',[RegisterController::class, 'validateSignup']);


Route::get('/logout/{key}',[crudcontroller::class, 'endSession']);


Route::get('/',[crudcontroller::class, 'displayHome']);
Route::get('/home',[crudcontroller::class, 'displayHome']);


//add appointment user
Route::post('/home/add/{id}',[crudcontroller::class, 'insertAptmnt']);

//edit user in admin
Route::get('/home/editUser/{id}',[crudcontroller::class, 'editUserShow']);

Route::post('/home/updateUser/{id}',[crudcontroller::class, 'updateUser']);

Route::get('/home/deleteUser/{id}',[crudcontroller::class, 'deleteUser']);


Route::get('/editAppointment/{id}',[crudcontroller::class, 'editAptmntShow']);

Route::post('/updateAptmnt/{id}',[crudcontroller::class, 'updateAptmnt']);

Route::get('/deleteAppointment/{id}',[crudcontroller::class, 'deleteAptmnt']);

Route::get('/pending',[crudcontroller::class, 'pendingShow']);

Route::get('/accepted',[crudcontroller::class, 'acceptedShow']);

Route::get('/completed',[crudcontroller::class, 'completedShow']);

Route::get('/declined/{id}',[crudcontroller::class, 'declinedAppointment']);

Route::get('/accepted/{id}',[crudcontroller::class, 'acceptedAppointment']);

Route::get('/completed/{id}',[crudcontroller::class, 'completedAppointment']);


