@extends('master')

@section('title')
    <title>Login</title>
@endsection

@section('content')


<div class="container-fluid my-5" style="height: 540px;">
    <div class="row d-flex justify-content-center">
        <div class="col-sm-6">
            <div class="container bg-light p-5 border">
                <form action="/login" method="post">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                    <div class="row text-center">
                        <div class="col-sm-12">
                            <h1>Login</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            @if (count($errors) > 0)
                            <div class = "alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>   
                                    @endforeach
                                </ul>
                            </div>@endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mx-auto">
                            <label for="" class="form-label">Username<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="username">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mx-auto">
                            <label for="" class="form-label">Password<span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>




                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-success w-100">Login</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <span>Don't have an account? Sign up </span><a href="/signup" class="text-primary">here.</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection