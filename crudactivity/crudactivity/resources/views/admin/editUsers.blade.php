@extends('master')

@section('title')
    <title>Home</title>
@endsection

@section('content')

  <script>
    $(function(){
      $("#example").DataTable();

      $("#tableAppointment").DataTable();

      //$('#exampleModal').modal("show");

      $('#exampleModal').modal("show",{
        backdrop: 'static',
        keyboard: false
        })  

      

    });
  </script>


  
  <!-- Modal -->
  <div class="modal hide" id="exampleModal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit User Information</h5>
        </div>
        <div class="modal-body">
          <form action="/home/updateUser/{{ $user[0]->user_id }}" method="post">
              <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

              <div class="row">
                  <div class="col-sm-12">
                      @if (count($errors) > 0)
                      <div class = "alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>   
                              @endforeach
                          </ul>
                      </div>@endif
                  </div>
              </div>

              <div class="row">
                  <div class="col-sm-12 mx-auto">
                      <label for="" class="form-label">First name<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" name="fname" value="{{ $user[0]->firstname }}" required>
                  </div>
              </div>

              <div class="row">
                <div class="col-sm-12 mx-auto">
                    <label for="" class="form-label">Last name<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="lname" value="{{ $user[0]->lastname }}" required>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 mx-auto">
                    <label for="" class="form-label">Email<span class="text-danger">*</span></label>
                    <input type="email" class="form-control" name="email" value="{{ $user[0]->email }}" required>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 mx-auto">
                    <label for="" class="form-label">Username<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="username" value="{{ $user[0]->username }}" required>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 mx-auto">
                    <label for="" class="form-label">User Type<span class="text-danger">*</span></label>
                    <select class="form-select" name="userType">
                        <option value="User" <?php if($user[0]->user_id == "User") echo "selected";?>>User</option>
                        <option value="Admin" <?php if($user[0]->user_type == "Admin") echo "selected";?>>Admin</option>
                      </select>
                </div>
              </div>


        
        </div>
        <div class="modal-footer">
            <a href="/home" class="btn btn-secondary">Close</a>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>

      </form>
      </div>
    </div>
  </div>

  <div class="container text-end">
      <div class="row">
          <div class="col-sm-12">
              <span>Welcome </span>
              <span class="fw-bold"> {{ session()->get('users')[0]->user_type }},</span>
              <span class="me-5"> {{ session()->get('users')[0]->firstname }}  {{ session()->get('users')[0]->lastname }}.</span>
              <a class="btn btn-danger" href="/logout/users">Logout</a>
          </div>
      </div>
  </div>
    
  <div class="container bg-light p-5 my-5 border" style="height:540px;">
    <div class="row">
        <div class="col-sm-6">
            <h1>Users</h1>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <table id="tableAppointment" class="table table-hover">
                <thead>
                  <tr class="table-dark">
                    <th>ID</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                    <th>User Type</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($user as $users)
                  <tr>
                  <td>{{ $users->user_id }}</td>
                  <td>{{ $users->firstname }}</td>
                  <td>{{ $users->lastname }}</td>
                  <td>{{ $users->email }}</td>
                  <td>{{ $users->user_type }}</td>
                  <td>
                    <div class="text-center">
                      <a href="#"> <i class="fa fa-edit fa-2x text-warning" aria-hidden="true"></i></a>
                      <a href="#" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                    </div>
                    
                  </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
    </div>

  </div>

@endsection