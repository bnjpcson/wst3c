@extends('master')

@section('title')
    <title>Home</title>
@endsection

@section('content')

  <script>
    $(function(){
      $("#userTbl").DataTable();
    });
  </script>



  <div class="container text-end">
      <div class="row">
          <div class="col-sm-12">
              <span>Welcome </span>
              <span class="fw-bold"> {{ session()->get('users')[0]->user_type }},</span>
              <span class="me-5"> {{ session()->get('users')[0]->firstname }}  {{ session()->get('users')[0]->lastname }}.</span>
              <a class="btn btn-danger" href="/logout/users">Logout</a>
          </div>
      </div>
  </div>

  
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="/home" class="text-dark">Users</a>
                <span>|</span>
                <a href="/pending">Pending Appointments</a>
                <span>|</span>
                <a href="/accepted">Accepted Appointments</a>
                <span>|</span>
                <a href="/completed">Completed Appointments</a>
            </div>
        </div>
    </div>
    
  <div class="container bg-light p-5 my-5 border" style="height:540px;">
    <div class="row">
        <div class="col-sm-6">
            <h1>Users Table</h1>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <table id="userTbl" class="table table-hover">
                <thead>
                  <tr class="table-dark">
                    <th>ID</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                    <th>User Type</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users1 as $user)
                  <tr>
                  <td>{{ $user->user_id }}</td>
                  <td>{{ $user->firstname }}</td>
                  <td>{{ $user->lastname }}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->user_type }}</td>
                  <td>
                    <div class="text-center">
                      <a href="/home/editUser/{{ $user->user_id }}"> <i class="fa fa-edit fa-2x text-warning" aria-hidden="true"></i></a>
                      <a href="/home/deleteUser/{{ $user->user_id }}" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                    </div>
                    
                  </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
    </div>

  </div>

@endsection