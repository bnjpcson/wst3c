@extends('master')

@section('title')
    <title>Admin | Pending Appointments</title>
@endsection

@section('content')

    <script>
        $(function(){
        $("#aptmntTbl").DataTable();
        });
    </script>

  <div class="container text-end">
      <div class="row">
          <div class="col-sm-12">
              <span>Welcome </span>
              <span class="fw-bold"> {{ session()->get('users')[0]->user_type }},</span>
              <span class="me-5"> {{ session()->get('users')[0]->firstname }}  {{ session()->get('users')[0]->lastname }}.</span>
              <a class="btn btn-danger" href="/logout/users">Logout</a>
          </div>
      </div>
  </div>

  
  <div class="container">
    <div class="row">
        <div class="col">
            <a href="/home">Users</a>
            <span>|</span>
            <a href="/pending" class="text-dark">Pending Appointments</a>
            <span>|</span>
            <a href="/accepted">Accepted Appointments</a>
            <span>|</span>
            <a href="/completed">Completed Appointments</a>
        </div>
    </div>
</div>
    
  <div class="container bg-light p-5 my-5 border" style="">

    <div class="row">
      <div class="col-sm-12">
          @if (count($errors) > 0)
          <div class = "alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>   
                  @endforeach
              </ul>
          </div>@endif
      </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <h1>Pending Appointments</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <table id="aptmntTbl" class="table table-hover">
                <thead>
                  <tr class="table-dark">
                    <th>Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Description</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($aptmnts as $aptmnt)
                  <tr>
                  <td>{{ $aptmnt->name }}</td>
                  <td>{{ $aptmnt->date }}</td>
                  <td>{{ $aptmnt->time }}</td>
                  <td>{{ $aptmnt->description }}</td>
                  <td>
                    <div class="text-center">
                      <a href="/declined/{{ $aptmnt->id }}"> <i class="fa fa-close fa-2x text-danger" aria-hidden="true"></i></a>
                      <a href="/accepted/{{ $aptmnt->id }}" class="ms-2" > <i class="fa fa-check fa-2x text-success" aria-hidden="true"></i></a>
                    </div>
                    
                  </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
    </div>

  </div>

@endsection