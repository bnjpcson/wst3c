<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Boostrap 5 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">

    {{-- Datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    {{-- Datatables --}}

    {{-- Title --}}
    @yield('title')


</head>
<body>
    @yield('content')



    <div class="container-fluid bg-dark mt-5 text-white">
        <div class="row">
            <div class="col-sm-6 mx-auto">
                <div class="row pt-5 d-flex justify-content-center text-center">
                    <div class="col-sm-12">
                        <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
                        <i class="fa fa-instagram fa-2x px-5" aria-hidden="true"></i>
                        <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="row pb-5 pt-3 d-flex justify-content-center text-center">
                    <span>
                        Benjie Pecson | Laravel - CRUD Activity
                    </span>                  
                </div>

            </div>
        </div>
        
    </div>
</body>
</html>