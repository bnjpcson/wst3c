@extends('master')

@section('title')
    <title>Sign Up</title>
@endsection

@section('content')


<div class="container-fluid my-5" style="">
    <div class="row d-flex justify-content-center">
        <div class="col-sm-6">
            <div class="container bg-light p-5 border">
                <form action="/signup" method="post">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                    <div class="row text-center">
                        <div class="col-sm-12">
                            <h1>Sign Up</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            @if (count($errors) > 0)
                            <div class = "alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>   
                                    @endforeach
                                </ul>
                            </div>@endif
                        </div>
                    </div>

                    


                    <div class="row">
                        <div class="col-sm-12 mx-auto">
                            <label for="" class="form-label">First Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="fname">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mx-auto">
                            <label for="" class="form-label">Last Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="lname">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mx-auto">
                            <label for="" class="form-label">Email Address<span class="text-danger">*</span></label>
                            <input type="email" class="form-control" name="email">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mx-auto">
                            <label for="" class="form-label">Username<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="username">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mx-auto">
                            <label for="" class="form-label">Password<span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="password1">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mx-auto">
                            <label for="" class="form-label">Confirm Password<span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="password2">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-success w-100">Sign Up</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <span>Already have an account? Login </span><a href="/login" class="text-primary">here.</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection