@extends('master')

@section('title')
    <title>Home</title>
@endsection

@section('content')

  <script>
    $(function(){
      $("#example").DataTable();
      $("#tableAppointment").DataTable();
    });
  </script>


  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add an appointment</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="/home/add/{{ session()->get('users')[0]->user_id }}" method="post">
              <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

              

              <div class="row">
                  <div class="col-sm-12 mx-auto">
                      <label for="" class="form-label">Date<span class="text-danger">*</span></label>
                      <input type="date" class="form-control" name="date" required>
                  </div>
              </div>

              <div class="row">
                <div class="col-sm-12 mx-auto">
                  <label for="" class="form-label">Time<span class="text-danger">*</span></label>
                  <select class="form-select" aria-label="Default select example" name="time" required>
                    <option value="08:00 AM - 10:00 AM">08:00 AM - 10:00 AM</option>
                    <option value="10:00 AM - 12:00 AM">10:00 AM - 12:00 AM</option>
                    <option value="01:00 PM - 03:00 PM">01:00 PM - 03:00 PM</option>
                    <option value="03:00 PM - 05:00 PM">03:00 PM - 05:00 PM</option>
                  </select>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 mx-auto">
                    <label for="" class="form-label">Description<span class="text-danger">*</span></label>
                    <textarea class="form-control" rows="3" required name="desc"></textarea>
                </div>
              </div>


        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add</button>
        </div>

      </form>
      </div>
    </div>
  </div>

  <div class="container text-end">
      <div class="row">
          <div class="col-sm-12">
              <span>Welcome </span>
              <span class="fw-bold"> {{ session()->get('users')[0]->user_type }},</span>
              <span class="me-5"> {{ session()->get('users')[0]->firstname }}  {{ session()->get('users')[0]->lastname }}.</span>
              <a class="btn btn-danger" href="/logout/users">Logout</a>
          </div>
      </div>
  </div>
    
  <div class="container bg-light p-5 my-5 border" style="">

    <div class="row">
      <div class="col-sm-12">
          @if (count($errors) > 0)
          <div class = "alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>   
                  @endforeach
              </ul>
          </div>@endif
      </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <h1>Appointments</h1>
        </div>

        <div class="col-sm-6 my-auto text-end">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Add Appointment
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <table id="tableAppointment" class="table table-hover">
                <thead>
                  <tr class="table-dark">
                    <th>Date</th>
                    <th>Time</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($aptmnts as $aptmnt)
                  <tr>
                  <td>{{ $aptmnt->date }}</td>
                  <td>{{ $aptmnt->time }}</td>
                  <td>{{ $aptmnt->description }}</td>
                  <td>{{ $aptmnt->status }}</td>
                  <td>
                    <div class="text-center">
                      <a href="/editAppointment/{{ $aptmnt->id }}"> <i class="fa fa-edit fa-2x text-warning" aria-hidden="true"></i></a>
                      <a href="/deleteAppointment/{{ $aptmnt->id }}" class="ms-2" > <i class="fa fa-trash fa-2x text-danger" aria-hidden="true"></i></a>
                    </div>
                    
                  </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
    </div>

  </div>

@endsection