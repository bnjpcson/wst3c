<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class crudcontroller extends Controller
{
    //

    
    public function endSession(Request $request, $key){

        $request->session()->pull($key, 'default');
        
        $request->session()->forget($key);
 
        $request->session()->flush();

        // return redirect('home');

        $users = DB::select('select * from users');
            // return view('stud_view',['users'=>$users]);

        return redirect('home');
    }

       
    public function displayHome(Request $request){
        
        if ($request->session()->has('users')) {
            //
            // $users = DB::select('select * from users');
            // return view('home')->with('users1', $users);
            // // return redirect('home');

            $user = session('users');

            
            if($user[0]->user_type == "Admin"){//admin

                $users = DB::select('select * from users');
                return view('admin.home')->with('users1', $users);

            }else{ //user


                $aptmnts = DB::select('select * from appointment WHERE user_id = ?', [$user[0]->user_id]);
                return view('home')->with('aptmnts', $aptmnts);

            }
        }else{
            return redirect('login');
        }
       
    }




    public function insertAptmnt(Request $request, $id){

        $this->validate($request,[
            'date'=>'required',
            'time'=>'required',
            'desc'=>'required'
        ]);

        $date = $request->input('date');
        $time = $request->input('time');
        $desc = $request->input('desc');


        $aptmnt = DB::select("select * from appointment WHERE date = ? AND time = ?",[$date, $time]);

        if($aptmnt){
            $users = DB::select('select * from users');
            return redirect('home')->withErrors(["Invalid"=>"Conflict in Date and Time"]);
        }else{
            $user = DB::select("select * from users WHERE user_id = ?", [$id]);

            $name = $user[0]->firstname." ".$user[0]->lastname;

            DB::insert('insert into appointment (user_id, name, date, time, description, status) values(?,?,?,?,?,?)',[$id, $name, $date, $time, $desc, "Pending"]);

            // return redirect('home');

            $users = DB::select('select * from users');
                // return view('stud_view',['users'=>$users]);

            return redirect('home');
        }


        


    }

    public function editUserShow(Request $request, $id){

        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            if($user[0]->user_type == "Admin"){//admin

                return redirect('login');

            }else{ //user
                $users = DB::select("select * from users WHERE user_id = ?", [$id]);
                return view('admin.editUsers')->with('user', $users);
            }
        }else{
            return redirect('login');
        }
    }

    public function updateUser(Request $request, $id){


        $this->validate($request,[
            'fname'=>'required',
            'lname'=>'required',
            'email'=>'required',
            'username'=>'required',
            'userType'=>'required'
        ]);

        $fname = $request->input('fname');
        $lname = $request->input('lname');
        $email = $request->input('email');
        $username = $request->input('username');
        $userType = $request->input('userType');

        DB::update('update users set firstname = ?, lastname = ?, email = ?, username = ?, user_type = ? where user_id = ?',[$fname, $lname, $email, $username, $userType, $id]);
        echo "Record updated successfully.<br/>";
        echo '<a href = "/home">Click Here</a> to go back.';
    }

    public function deleteUser(Request $request, $id){


        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            if($user[0]->user_type == "Admin"){//admin

                return redirect('login');

            }else{ //user
                DB::delete('delete from users where user_id = ?',[$id]);
                echo "Record deleted successfully.<br/>";
                echo '<a href = "/home">Click Here</a> to go back.';
            }
        }else{
            return redirect('login');
        }

    }



    

    

    

    public function editAptmntShow(Request $request, $id){

        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            if($user[0]->user_type == "Admin"){//admin

                return redirect('login');

            }else{ //user
                $aptmnts = DB::select("select * from appointment WHERE id = ?", [$id]);
                return view('editAptmnt')->with('aptmnts', $aptmnts);
            }
        }else{
            return redirect('login');
        }


    }

    

    public function updateAptmnt(Request $request, $id){


        $this->validate($request,[
            'date'=>'required',
            'time'=>'required',
            'desc'=>'required'
        ]);

        $date = $request->input('date');
        $time = $request->input('time');
        $desc = $request->input('desc');




        $aptmnt = DB::select("select * from appointment WHERE date = ? AND time = ?",[$date, $time]);

        if($aptmnt){
            return redirect('home')->withErrors(["Invalid"=>"Conflict in Date and Time"]);
        }else{
            DB::update('update appointment set date = ?, time = ?, description = ? where id = ?',[$date, $time, $desc, $id]);
            echo "Record updated successfully.<br/>";
            echo '<a href = "/home">Click Here</a> to go back.';
        }
    }

    public function deleteAptmnt(Request $request, $id){

        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            DB::delete('delete from appointment where id = ?',[$id]);
            echo "Record deleted successfully.<br/>";
            echo '<a href = "/home">Click Here</a> to go back.';
            
        }else{
            return redirect('login');
        }

    }

    public function pendingShow(Request $request){

        

        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            if($user[0]->user_type == "Admin"){//admin

                $aptmnts = DB::select("select * from appointment WHERE status = ?", ["Pending"]);
                return view('admin.pendingAptmnt')->with('aptmnts', $aptmnts);

            }else{ //user
                

                return redirect('login');
            }
        }else{
            return redirect('login');
        }

    }

    public function acceptedShow(Request $request){

        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            if($user[0]->user_type == "Admin"){//admin

                $aptmnts = DB::select("select * from appointment WHERE status = ?", ["Accepted"]);
                return view('admin.acceptedAptmnt')->with('aptmnts', $aptmnts);

            }else{ //user
                

                return redirect('login');
            }
        }else{
            return redirect('login');
        }

    }

    public function completedShow(Request $request){

        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            if($user[0]->user_type == "Admin"){//admin

                $aptmnts = DB::select("select * from appointment WHERE status = ?", ["Completed"]);
                return view('admin.completedAptmnt')->with('aptmnts', $aptmnts);

            }else{ //user

                return redirect('login');
            }
        }else{
            return redirect('login');
        }


    }

    

    public function declinedAppointment(Request $request, $id){

        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            if($user[0]->user_type == "Admin"){//admin

                DB::update('update appointment set status = ? where id = ?',["Declined", $id]);
                echo "Record updated successfully.<br/>";
                echo '<a href = "/home">Click Here</a> to go back.';

            }else{ //user

                return redirect('login');
            }
        }else{
            return redirect('login');
        }

    }

    public function acceptedAppointment(Request $request, $id){

        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            if($user[0]->user_type == "Admin"){//admin

                $aptmnts = DB::select("select * from appointment WHERE status = ?", ["Accepted"]);

                $aptmnt = DB::select("select * from appointment WHERE status = ?", ["Pending"]);

                if(count($aptmnts)>=4){
                    return view('admin.pendingAptmnt',["aptmnts" => $aptmnt])->withErrors(["Invalid"=>"Maximum number of appointments has been reached."]);
                }

                DB::update('update appointment set status = ? where id = ?',["Accepted", $id]);
                echo "Record updated successfully.<br/>";
                echo '<a href = "/pending">Click Here</a> to go back.';

            }else{ //user

                return redirect('login');
            }
        }else{
            return redirect('login');
        }

        

        

    }

    public function completedAppointment(Request $request, $id){


        if ($request->session()->has('users')) {
          
            $user = session('users');
            
            if($user[0]->user_type == "Admin"){//admin

                DB::update('update appointment set status = ? where id = ?',["Completed", $id]);
                echo "Record updated successfully.<br/>";
                echo '<a href = "/accepted">Click Here</a> to go back.';

            }else{ //user

                return redirect('login');
            }
        }else{
            return redirect('login');
        }
        
    }

}
