<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RegisterController extends Controller
{
    //

    public function signupShow(Request $request){
        

        if ($request->session()->has('users')) {

            return redirect('home');

        }else{
            return view('signup');
        }
        
    }

    public function validateSignup(Request $request){
       
        $this->validate($request,[
        'fname'=>'required',
        'lname'=>'required',
        'email'=>'required',
        'username'=>'required',
        'password1'=>'required',
        'password2'=>'required'

        ]);

        $fname = $request->input('fname');
        $lname = $request->input('lname');
        $email = $request->input('email');
        $username = $request->input('username');
        $password1 = md5($request->input('password1'));
        $password2 = md5($request->input('password2'));

        $user = DB::select("select * from users WHERE email = ?", [$email]);


        if($user){

            return view('signup')->withErrors(["Invalid"=>"Email Already exists."]);

        }elseif($password1 != $password2){

            return view('signup')->withErrors(["Invalid"=>"Two Passwords do not match."]);

        }else{

            DB::insert('insert into users (firstname, lastname, email, username, password, user_type) values(?,?,?,?,?,?)',[$fname, $lname, $email, $username, $password1, "User"]);

            return redirect('login');
            
        }
    }
}
