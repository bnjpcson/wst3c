<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LoginController extends Controller
{
    //

    public function displayLogin(Request $request){

        if ($request->session()->has('users')) {
            //
            // return redirect('home');

            $user = session('users');

            
            if($user[0]->user_type == "Admin"){//admin

                $users = DB::select('select * from users');
                // return view('stud_view',['users'=>$users]);
    
                return redirect('home')->with('users1', $users);

            }else{ //user

                $users = DB::select('select * from users');
                // return view('stud_view',['users'=>$users]);
    
                return redirect('home')->with('users1', $users);

            }

           
        }else{
            return view('login');
        }
    }

    public function validateLogin(Request $request){
       
        $this->validate($request,[
        'username'=>'required|max:8',
        'password'=>'required'
        ]);

        $username = $request->input('username');
        $password = md5($request->input('password'));
        $user = DB::select("select * from users WHERE username = ? AND password = ?", [$username, $password]);
        if($user){
            //return view('home',['users' => $user]);
            
            $request->session()->put('users', $user);
            //return redirect('home');
            return redirect('home')->with('users1', $user);

        }else{
            return view('login')->withErrors(["Invalid"=>"Invalid Username or Password"]);
        }
    }
}
