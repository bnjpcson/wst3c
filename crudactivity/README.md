**CRUD Activity**

---

## Instructions
Create a program similar to appointment system  with the following requirements:
-login(username and password)
-registration(use general information)
-appointment
-create a 2-10 video explaining the features you have plus the technology applied.
-make sure admin can approve request of appointment with checking if time is conflict or not

note: appointment will be auto approved once detected that the time is not conflict.
the doctor can only accommodate 4 client in total and max of 2 hours each.
validation for the form is required accordingly. Use bootstrap for the design or any. Prospect company should be unique.

email of the user should be unique

Requirements for submission
-turn in screenshot of each part of your output
-push all your output in your repo (source code)
-prepare for evaluation of your output next meeting